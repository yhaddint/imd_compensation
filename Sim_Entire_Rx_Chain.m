% 04/11/2016
% the self-interference of blocker is
% z1_tilde = z1*(beta1+3*beta3(0.5|z1|^2+|z2|^2))
% when power of z1 and z2 are significantly different, it is possible to
% estimate stronger one 

%-------------------------------------
% Script control parameter
%-------------------------------------
clear;clc;close all
rand('seed',4)
RF_nonlinearity = 1; % Turn-on LNA nonlinearity simulation
IQimbalance = 0; % Turn-on IQ imbalance simulation
BB_nonlinearity = 1; % Turn-on baseband amplifiers nonlinearity simulation

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 2;
M = 10;
N = 125;
channel_num = 300;
SampleNumberAve = 300;
stat_num = 2e2;
P = 1e4*3;
sig_length = M*SampleNumberAve*stat_num;
FFT_size = 2^13;

%-------------------------------------
% RF impairment model
% IQ imbalance parameter from [Grimm2014]
%-------------------------------------
if RF_nonlinearity 
    beta1 = 5.62; % LNA gain 15 dB
    beta3 = -749.33; % IIP3 is -10dBm
else
    beta1 = 5.62; % LNA gain 15 dB
    beta3 = 0; % IIP3 is infty
end

if IQimbalance
    gm = 0.99; % IQ imbalance gain mismatch
    phim = 0.0628; %IQ imbalance phase mismatch
else
    gm = 1; % IQ imbalance gain mismatch
    phim = 0;%0.0628; %IQ imbalance phase mismatch
end
IQ1 = (1+gm*exp(-1j*phim))/2; % y = x*IQ1+conj(x)*IQ2
IQ2 = (1-gm*exp(1j*phim))/2; % y = x*IQ1+conj(x)*IQ2

if BB_nonlinearity
    beta1_BB = 3.16; % BB amplifier gain is 10dB
    beta3_BB = -10.583; % BB amplifier IIP3 is 6dBm
else
    beta1_BB = 3.16; % BB amplifier gain is 10dB
    beta3_BB = 0; % BB amplifier IIP3 is infty
end

%%
QAM_level = 4;
[ sig ] = get_waveform( QAM_level, sig_length, upsam, blk_num )

% OFDMcarrier = 1024;
% for ii=1:floor(P/10/OFDMcarrier)+1
%     OFDMsymb = (randi(2,OFDMcarrier,1)*2-3)+1j*(randi(2,OFDMcarrier,1)*2-3);
%     OFDMsamp = ifft(OFDMsymb);
%     OFDM_env((ii-1)*OFDMcarrier+1:ii*OFDMcarrier,1) = OFDMsamp;
% end
% OFDM_env_oversample = kron(OFDM_env(1:P/10),ones(10,1));
% bhi = fir1(200,0.09/3,'low');
% OFDM_env_rx = filter(bhi,1,OFDM_env_oversample);
% OFDM_env_rx_norm = OFDM_env_rx(101:end)./norm(OFDM_env_rx(101:end)).*sqrt(length(OFDM_env_rx(101:end)));


%% BLK generating
    
    start_point = randi(L-P-1,blk_num,1);
    for ii=1:blk_num
        sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
    end
    pow_dBm = [-35, -35];
    pow = 10.^((pow_dBm-30)./10);

    ampl = sqrt(pow*2*50);
    
    f1 = 27.5;
    f2 = 12.5;
    
    PhaseShift1 = exp(1j*rand*2*pi);
    PhaseShift2 = exp(1j*rand*2*pi);
    
    carrier1 = exp(1j*(f1/channel_num*2*pi*(0:P-1)'));
    carrier2 = exp(1j*(f2/channel_num*2*pi*(0:P-1)'));
%     carrier3 = exp(1j*(f3/channel_num*2*pi*(1:P)'));
    


    z1_base = ampl(1)*sig_cache(:,1);
    z2_base = ampl(2)*sig_cache(:,2);
    
    z1 = z1_base.*carrier1*PhaseShift1;
    z2 = z2_base.*carrier2*PhaseShift2;
%% AWGN
    sig_length = length(z1);
    noise = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
    pow_Noise_dB = -90;
    pow_noise = 10^((pow_Noise_dB-30)/10);
    noise_ampl = sqrt(pow_noise*2*50);
%% LNA non-linearity model
     y_f1 = beta1*z1 + 3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2));
     y_f2 = beta1*z2 + 3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2));
     y_f112 = 3/2*beta3*z1.*z1.*conj(z2); % 2f1-f2
     y_f221 = 3/2*beta3*z2.*z2.*conj(z1); % 2f2-f1
     AWGN = noise*noise_ampl;
     
     y_LNAout = y_f1+y_f2+y_f112+y_f221+AWGN;
    
%% IQ imbalance model
%     y_IQ_f1_i = real(y_f1);
%     y_IQ_f2_i = real(y_f2);
%     y_IQ_f112_i = real(y_f112);
%     y_IQ_f221_i = real(y_f221);
%     
%     y_IQ_f1_q = gm*cos(phim)*imag(y_f1) - gm*sin(phim)*real(y_f1);
%     y_IQ_f2_q = gm*cos(phim)*imag(y_f2) - gm*sin(phim)*real(y_f2);
%     y_IQ_f112_q = gm*cos(phim)*imag(y_f112) - gm*sin(phim)*real(y_f112);
%     y_IQ_f221_q = gm*cos(phim)*imag(y_f221) - gm*sin(phim)*real(y_f221);
    y_IQ_i = real(y_LNAout);
    y_IQ_q = gm*cos(phim)*imag(y_LNAout) -gm*sin(phim)*real(y_LNAout);

%% BB non-linearity model
    bhi0 = fir1(200,0.33,'low');
%     freqz(bhi0)
    y_BB_real = beta1_BB * y_IQ_i + beta3_BB * y_IQ_i.^3;
    y_BB_real_temp = filter(bhi0,1,y_BB_real);
    y_BB_imag = beta1_BB * y_IQ_q + beta3_BB * y_IQ_q.^3;
    y_BB_imag_temp = filter(bhi0,1,y_BB_imag);
    y_BB = y_BB_real_temp(101:end)+1j*y_BB_imag_temp(101:end);
    y_BBout = y_BB(1:3:end);
    
%% quick PSD watch and power verification
% ss_index = linspace(-50,50,FFT_size);
% y_uncomp_Fdomain = 20*log10(abs(fft(y_BBout(1:FFT_size).*hann(FFT_size)))/FFT_size);
% ss_uncomp = 10*log10(27)+10*log10(81.92)...
%             +[y_uncomp_Fdomain(FFT_size/2+1:end);y_uncomp_Fdomain(1:FFT_size/2)];
% figure(88)
% plot(ss_index,ss_uncomp)
% grid on
% ylim([-120,0])
% xlabel('Frequency (MHz)')
% ylabel('Power Density (dBm/Hz)')
% title('Non-compensated PSD')
% 
% ss_index = linspace(-50,50,FFT_size);
% SOI_index = find(and((ss_index>(12.5-1)), (ss_index<(12.5+1))));
% xxx = abs(fft(y_BBout(1:FFT_size).*hann(FFT_size)))/FFT_size;
% ss_nocomp = [xxx(FFT_size/2+1:end);xxx(1:FFT_size/2)];
% powBOI_nocomp = 10*log10(2.7e1*sum(ss_nocomp(SOI_index).^2))

%% activate two or three BLKs
    
bhi = fir1(200,0.08,'low');


carrier1 = exp(1j*(f1/100*2*pi*(0:P-1)'));
carrier2 = exp(1j*(f2/100*2*pi*(0:P-1)'));
    
temp = filter(bhi,1,y_BBout.*conj(carrier1(1:length(y_BBout))));
z1_tilde_filter = temp(101:FFT_size+100).*carrier1(1:FFT_size);

temp = filter(bhi,1,y_BBout.*conj(carrier2(1:length(y_BBout))));
z2_tilde_filter = temp(101:FFT_size+100).*carrier2(1:FFT_size);
%%
IQcomp = IQ1;
zxx = beta1*beta1_BB*z2(1:3:end)...
      +3*beta3*beta1_BB*z2(1:3:end).*(0.5*z2(1:3:end).*conj(z2(1:3:end))+z1(1:3:end).*conj(z1(1:3:end)));
% zxx = beta1*beta1_BB*z2(1:3:end);
% figure
% plot(abs(z2_tilde_filter(1:FFT_size)));hold on
% plot(abs(IQcomp*zxx(1:FFT_size)));hold on
% legend('z2 tilde filter','z2 tilde constuct')
figure
semilogy(abs(z2_tilde_filter(1:FFT_size)-IQcomp*zxx(1:FFT_size)));hold on
hold on
grid on

%% quick PSD watch and power verification
ss_index = linspace(-50,50,FFT_size);
y_uncomp_Fdomain = 20*log10(abs(fft(z2(1:FFT_size).*hann(FFT_size)))/FFT_size);
ss_uncomp = 10*log10(27)+10*log10(81.92)...
           +[y_uncomp_Fdomain(FFT_size/2+1:end);y_uncomp_Fdomain(1:FFT_size/2)];
figure(88)
plot(ss_index,ss_uncomp)
grid on
xlim([-50,50])
ylim([-120,0])
xlabel('Frequency (MHz)')
ylabel('Power Density (dBm/Hz)')
title('Non-compensated PSD')
%% quick PSD from LPF to watch IMD around blockers

% temp9 = 20*log10(abs(fft(z1_tilde_filter)/6e3));
% figure
% plot([temp9(3001:end);temp9(1:3000)],'b');hold on
% temp9 = 20*log10(abs(fft(z2_tilde_filter)/6e3));
% plot([temp9(3001:end);temp9(1:3000)],'r')

%% get an idea how to make time alignment after LPF
    
%     z1_tilde_theo = beta1*z1+beta3*z1.*(abs(z1).^2+abs(z2).^2);
%     z2_tilde_theo = beta1*z2+beta3*z2.*(abs(z1).^2+abs(z2).^2);
%     
%     figure
%     plot(abs((z2_tilde_theo(1:5e2))),'b');hold on
%     plot(abs((z2_tilde_filter(1e2:6e2))),'r');hold on
%     figure
%     plot(20*log10(abs(fft(y_LNAout))/length(y_LNAout)),'r');hold on
%     
    %%
    
%     s_n0 = (abs(z1_tilde_filter).^2+abs(z2_tilde_filter).^2)*3*beta3;
    s_n = (abs(z1_tilde_filter).^2)*3*beta3/2;

    k0 = zeros(FFT_size,1);
    k1 = zeros(FFT_size,1);
    k2 = zeros(FFT_size,1);
%     k3 = zeros(FFT_size,1);
    
    for jj=1:FFT_size
        [k1(jj),k2(jj)] = block_estimation_v1(beta1,beta3,abs(z1_tilde_filter(jj)),abs(z2_tilde_filter(jj)));
    end
    
    
%     k3 = beta1+3*beta3*(abs(z1_tilde_filter)./k1).^2;
    
    z0_tilde_recons1 = (z1_tilde_filter./k1).*(3/2*beta3*abs(z1_tilde_filter./k1).^2+3*beta3*abs(z2_tilde_filter./k2).^2);
    z0_tilde_recons2 = (z2_tilde_filter./k2).*(3/2*beta3*abs(z2_tilde_filter./k2).^2+3*beta3*abs(z1_tilde_filter./k1).^2);
    z0_tilde_recons21 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
    z0_tilde_recons12 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);
%     z0_tilde_recons23 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z3_tilde_filter./k3);
%     z0_tilde_recons32 = 3/2*beta3*(z3_tilde_filter./k3).^2.*conj(z2_tilde_filter./k2);
%     z0_tilde_recons13 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z3_tilde_filter./k3);

%     z0_tilde_recons21_close = 3*beta3/2*(z2_tilde_filter).^2.*conj(z1_tilde_filter)./k0.^3;
%     z0_tilde_recons12_close = 3*beta3/2*(z1_tilde_filter).^2.*conj(z2_tilde_filter)./k0.^3;
    
    %% theta from LMS filter
    
%     stepsize = 6e10;
    stepsize = 5e10;
    
    % LMS for CMD 112
    vofn = 3*beta3/2*z1.*z1.*conj(z2);
    uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,1000);
    z0_tilde_recons12_LMS = thetaLMS(end)'.*uofn;
    
    % LMS for CMD 221
    vofn = 3*beta3/2*z2.*z2.*conj(z1);
    uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,1000);
    z0_tilde_recons21_LMS = thetaLMS(end)'.*uofn;
    
   
    
%     % LMS for CMD 223
%     vofn = 3*beta3/2*z2.*z2.*conj(z3);
%     uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons23_LMS = thetaLMS(1:FFT_size)'.*uofn;
% 
%     % LMS for CMD 332
%     vofn = 3*beta3/2*z3.*z3.*conj(z2);
%     uofn = 3/2*(z3_tilde_filter.*z3_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons32_LMS = thetaLMS(1:FFT_size)'.*uofn;
% 
%     % LMS for CMD 113
%     vofn = 3*beta3/2*z1.*z1.*conj(z3);
%     uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons13_LMS = thetaLMS(1:6e3)'.*uofn;

   
    %
%     k_true = beta1+beta3*(abs(z1).^2+abs(z2).^2);
%     orig_term = z0_tilde_filter(101:61e2);
%     cancel_term32 = beta3*z0_tilde_recons32./(k(1:60e2).^3);
%     cancel_term23 = beta3*z0_tilde_recons23./(k(1:60e2).^3);
%     cancel_term13 = beta3*z0_tilde_recons13./(k(1:60e2).^3);
    
    cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
%     cancel_term_close = z0_tilde_recons21_close+z0_tilde_recons12_close;
    cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS;
%         z_tilde_recons11a_LMS+z_tilde_recons11b_LMS;
%     cancel_term_LMS = z_tilde_recons11a_LMS+z_tilde_recons11b_LMS;
    
%     cancel_term = cancel_term21+cancel_term12+cancel_term23+cancel_term32+cancel_term13;
%     figure
%     plot(abs(orig_term));hold on
%     plot(abs(cancel_term1))
    
%     figure
%     plot(20*log10(abs(fft(orig_term))/6e3));hold on
%     plot(20*log10(abs(fft(orig_term-cancel_term1-cancel_term2))/6e3),'r');hold on
%     grid on
%     
%     figure
%     temp = 20*log10(abs(fft(orig_term-cancel_term1)/6e3));
%     plot([temp(3001:end);temp(1:3000)],'r');hold on
%     temp = 20*log10(abs(fft(cancel_term2))/6e3);
%     plot([temp(3001:end);temp(1:3000)],'g');hold on

    %% figure plotting
        ss_index = linspace(-50,50,FFT_size);
        SOI_index = find(and((ss_index>(-12.5-1)), (ss_index<(-12.5+1))));
        plot_scale = [-50,50,-120,10];
        figure
        subplot(311)
        y_uncomp_Fdomain = 20*log10(abs(fft(y_BBout(1:FFT_size).*hann(FFT_size))/FFT_size));
        
        % The total power reduces by 2.7 withhann window;
        % additional 10dB is used to convert volt into dBm in R=50om impedance
        % 81.92 is used to convert dBm/bin into dBm/MHz
        ss_uncomp = 10*log10(27)+10*log10(81.92)...
            +[y_uncomp_Fdomain(FFT_size/2+1:end);y_uncomp_Fdomain(1:FFT_size/2)];
        plot(ss_index, ss_uncomp);hold on
        plot(ss_index(SOI_index), ss_uncomp(SOI_index));hold on
        grid on
        axis(plot_scale);
        xlabel('Frequency (MHz)')
        ylabel('PSD (dBm/MHz)')
        title('Non-compensated PSD')
        
    
        subplot(312)
        y_comp_Fdomain_prop = 20*log10(abs(fft((y_BBout(1:FFT_size)-cancel_term(1:FFT_size)).*hann(FFT_size)))/FFT_size);
        ss_prop = 10*log10(27)+10*log10(81.92)...
            +[y_comp_Fdomain_prop(FFT_size/2+1:end);y_comp_Fdomain_prop(1:FFT_size/2)];
        plot(ss_index,ss_prop);hold on
        plot(ss_index(SOI_index),ss_prop(SOI_index));hold on
        hold on
        title('Proposed') 
        xlabel('Frequency (MHz)')
        ylabel('PSD (dBm/MHz)')
%         axis(plot_scale);
        grid on
        
        subplot(313)
        y_comp_Fdomain_LMS = 20*log10(abs(fft((y_BBout(1:FFT_size)-cancel_term_LMS(1:FFT_size)).*hann(FFT_size)))/FFT_size);
        ss_LMS = 10*log10(27)+10*log10(81.92)...
            +[y_comp_Fdomain_LMS(FFT_size/2+1:end);y_comp_Fdomain_LMS(1:FFT_size/2)];
        plot(ss_index,ss_LMS);hold on
        plot(ss_index(SOI_index),ss_LMS(SOI_index))
        hold on
        grid on
%         axis(plot_scale);
        xlabel('Frequency (MHz)')
        ylabel('PSD (dBm/MHz)')
        title('LMS') 
        grid on
%% watch for equivalent theta in
% 1) optimal compensation
% 2) proposed method
% 3) LMS

beta3_eq = beta3*(beta1./(beta1+3*beta3*(1/2*abs(z1.*conj(carrier1)).^2+abs(z2.*conj(carrier2)).^2))).^2.*(beta1./(beta1+3*beta3*(1/2*abs(z2.*conj(carrier2)).^2+abs(z1.*conj(carrier1)).^2)));
theta_mymethod = beta3*beta1^3./k1.^2./k2;
figure(99)
plot(10+10*log10(-4*beta1/3./beta3_eq),'b')
hold on
plot(10+10*log10(-4*beta1/3./theta_mymethod),'r')
hold on
plot(10+10*log10(-4*beta1/3./thetaLMS),'g--','linewidth',1)




