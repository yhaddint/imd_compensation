%-------------------------------------
% Script control parameter
%-------------------------------------
clear;clc;close all
rand('seed',4)
IQimbalance = 1; % Turn-on IQ imbalance simulation

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 1;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 10;
noise_pow_dBm = -90; % AWGN power in dBm
stat_num = 2e2;
P = 1e4*3;
sig_length = M*SampleNumberAve*stat_num;
FFT_size = 2^13;
f1 = 17.5;
sig_pow_dBm = -20;

%-------------------------------------
% RF impairment model
% IQ imbalance parameter from [Grimm2014]
%-------------------------------------
if IQimbalance
    gm = 0.99; % IQ imbalance gain mismatch
    phim = 0.0628; %IQ imbalance phase mismatch
else
    gm = 1; % IQ imbalance gain mismatch
    phim = 0; % IQ imbalance phase mismatch
end

%-------------------------------------
% Generate IQ samples of test waveform
%-------------------------------------
QAM_level = 4;
[ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, blk_num );

sig_pow = 10.^((sig_pow_dBm-17.5)./10); % the fixed offset is due to 50 om resistance
awgn = get_AWGN( sig_length, 10^(noise_pow_dBm/10) ); % AWGN
sig_ideal = get_BB_freq_shift( sig(:,1), f1, sig_pow, channel_num) + awgn;

%-------------------------------------
% Apply IQ Imbalance
%-------------------------------------
sig_out = get_IQimbalance(sig_ideal, gm, phim);

%-------------------------------------
% Estimate IQ imbalance Parameter
%-------------------------------------
N_IQest = 1e4;
[gm_hat, phim_hat] = run_IQest(sig_out, N_IQest);

%-------------------------------------
% Apply IQ imbalance Compensation Alg.
%-------------------------------------
% gm_hat = gm;
% phim_hat = phim;
sig_comp = run_IQcomp(sig_out, gm_hat, phim_hat);

%% Quick watch of PSD of signal w/ IQ imbalance
[ x_freq, PSD_actual ] = get_PSD(sig_ideal,FFT_size, channel_num);
figure
plot(x_freq, -41+PSD_actual);
hold on
grid on
xlabel('Freq (MHz)')
ylabel('PSD')
title('PSD of Signal w/o IQ imbalance')

%% Quick watch of PSD of signal w/ IQ imbalance
[ x_freq, PSD_actual ] = get_PSD(sig_out,FFT_size, channel_num);
figure
plot(x_freq, -41+PSD_actual);
hold on
grid on
xlabel('Freq (MHz)')
ylabel('PSD')
title('PSD of Signal w/ IQ imbalance')
%% Quick watch of PSD of signal w/ IQ imbalance and DSP compensation
[ x_freq, PSD_actual ] = get_PSD(sig_comp,FFT_size, channel_num);
figure
plot(x_freq, -41+PSD_actual);
hold on
grid on
xlabel('Freq (MHz)')
ylabel('PSD')
title('PSD of Imbalance Compensated Signal')