% clear;clc

v = (randn(1e4,1)+1j*randn(1e4,1))*sqrt((1e-6)/2);
z1_tilde = z1.*(beta1+3/2*beta3.*(abs(z1).^2+2*abs(z2).^2));
z2_tilde = z2.*(beta1+3/2*beta3.*(abs(z2).^2+2*abs(z1).^2));
z0_tilde = 3/2*beta3.*z1.*z1.*conj(z2)+beta1*v;

theta_true = beta3*(1./(1+3*beta3/2/beta1*(2*abs(z1).^2+abs(z2).^2))).^2.*...
    (1./(1+3*beta3/2/beta1*(abs(z1).^2+2*abs(z2).^2)));
q_modelchange = theta_true(2:end)-theta_true(1:end-1);
sigmaq2 = var(q_modelchange);

% w(1) = beta3+randn*sqrt(sigmaq2);
% for ii=1:1e4-1
%     w(ii+1) = w(ii)+randn*sqrt(sigmaq2);
% end
% 
% var(w(2:end)-w(1:end-1))
w = theta_true';

% uofn = (randn(1e4,1)+1j*randn(1e4,1))*sqrt((1e-14)/2);
uofn = (3/2/beta1^3)*((z1_tilde).^2.*conj(z2_tilde));
dofn = uofn.*w'+v;


stepsize = 6e12;

sigmau2 = var(uofn);
sigmav2 = var(v);

stepsize_opt = sqrt(sigmaq2/(sigmav2*sigmau2)+(sigmaq2^2/4/sigmav2))-sigmaq2/2/sigmav2;

MSE_theo = 10*log10((1/stepsize*sigmaq2+stepsize*sigmav2*sigmau2)/(2-2*stepsize*sigmau2))

z0_comp = theta_true.*(3/2/beta1^3).*(z1_tilde).^2.*conj(z2_tilde);
stable = stepsize*sigmau2

%% theta from LMS filter
    
%     stepsize = 6e10;
    
    
    % LMS for CMD 112
%     vofn = 3*beta3/2*z1.*z1.*conj(z2);
%     uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(dofn,uofn,stepsize,w(1),length(dofn));
    z0_tilde_recons12_LMS = thetaLMS'.*uofn;
    error_empr = (dofn-v)-z0_tilde_recons12_LMS;
    MSE_empr = 10*log10(var(error_empr(200:end)))
    %%
figure;
plot(w);hold on
plot(thetaLMS,'r')