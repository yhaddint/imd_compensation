% 04/11/2016
% the self-interference of blocker is
% z1_tilde = z1*(beta1+3*beta3(0.5|z1|^2+|z2|^2))
% when power of z1 and z2 are significantly different, it is possible to
% estimate stronger one 
clear;clc;

clf;close all


rand('seed',4)
blk_num = 8;
M = 10;
N = 100;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8;
L = M*SampleNumberAve*stat_num;



for ii = 1:blk_num
%     sig_r(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig_i(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig(:,ii) = (sig_r(:,ii)+sig_i(:,ii))/sqrt(2);
    upsam = SampleNumberAve;
    symbols = fix(L*2/upsam);
    
    clearvars data temp_data
    hmod = modem.qammod('M', 4, 'InputType', 'integer');
    hdesign  = fdesign.pulseshaping(upsam,'Square Root Raised Cosine');
    hpulse = design(hdesign);
    data = randi(4,symbols,1)-1;
    data = modulate(hmod, data);
    data = upsample(data,upsam);
    temp_data = conv(data,hpulse.Numerator);
%     unnormalized = real(temp_data(end-L+1:end));
%     sig_r(:,ii) = unnormalized./sqrt(sum(unnormalized.^2)/L);
%     unnormalized = imag(temp_data(end-L+1:end));
%     sig_i(:,ii) = unnormalized./sqrt(sum(unnormalized.^2)/L);
%     sig_i(:,ii) = zeros(size(sig_r(:,ii)));
%     sig(:,ii) = (sig_r(:,ii)+sig_i(:,ii))/sqrt(2);
    sig(:,ii) = temp_data(end-L+1:end)./sqrt(temp_data(end-L+1:end)'*temp_data(end-L+1:end)/L);

end


%% CMD generating
beta1 = 56.23;
beta3 = -7497.33;

theta_num = 20;
IIP3_pool = linspace(-12,-9,theta_num);
theta_pool = -4*beta1/3./(10.^((IIP3_pool-10)/20)).^2;
    
    
runtimes = 2e2;
% for runindex = 1:runtimes
%     if mod(runindex,10)==1
%         runindex/runtimes*100
%     end
    start_point = randi(L-P-1,blk_num,1);
    for ii=1:blk_num
        sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
    end
    pow_dB = ones(8,3)*(-20);
    pow_dB(:,2) = pow_dB(:,1)-10;
    pow_dB(:,3) = pow_dB(:,1)-10-3;
    onoff = rand(8,3);
%     pow_dB = rand(8,3)*20-40;
    pow = (10.^((pow_dB-17.5)./10)).*(onoff>0);
    
    f1 = 5;
    f2 = 15;
    f3 = 30;
    
    PhaseShift1 = exp(1j*rand*2*pi);
    PhaseShift2 = exp(1j*rand*2*pi);
    PhaseShift3 = exp(1j*rand*2*pi);
    
    carrier1 = exp(1j*(f1/channel_num*2*pi*(1:P)'));
    carrier2 = exp(1j*(f2/channel_num*2*pi*(1:P)'));
    carrier3 = exp(1j*(f3/channel_num*2*pi*(1:P)'));
    
    z1_base = kron(sqrt(pow(:,1)),ones(M*N,1)).*sig_cache(:,1);
    z2_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*sig_cache(:,2);
    z3_base = kron(sqrt(pow(:,3)),ones(M*N,1)).*sig_cache(:,3);

%     z1_base = kron(sqrt(pow(:,1)),hann(M*N)).*sig_cache(:,1);
%     z2_base = kron(sqrt(pow(:,2)),hann(M*N)).*sig_cache(:,2);
%     z3_base = kron(sqrt(pow(:,3)),hann(M*N)).*sig_cache(:,3);
    
    z1 = z1_base.*carrier1*PhaseShift1;
    z2 = z2_base.*carrier2*PhaseShift2;
    z3 = z3_base.*carrier3*PhaseShift3;
    
    %%
%     figure;
%     plot(20*log10(abs(fft(beta1*(z1+z2).*hann(M*N*8)))/(M*N*8)));
%     
    %%
    % z1 = [sig_cache(:,1)*sqrt(pow(1,1));sig_cache(:,2)*sqrt(pow(1,2));sig_cache(:,3)*sqrt(pow(1,3));sig_cache(:,4)*sqrt(pow(1,4))];
    % z2 = [sig_cache(:,5)*sqrt(pow(2,1));sig_cache(:,6)*sqrt(pow(2,2));sig_cache(:,7)*sqrt(pow(2,3));sig_cache(:,8)*sqrt(pow(2,4))];
    sig_length = length(z1);
%     pow_noise_dB = -100;
%     pow_noise = 10^(pow_noise_dB/10);

    noise = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
    pow_Noise_dB = -90;
    %%
    bhi = fir1(200,3/channel_num,'low');
%     freqz(bhi)

    y_LNAout = beta1*(z1+z2+z3)...
        +beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3))...
        +beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2)+z3.*conj(z3))...
        +beta3*z3.*(z1.*conj(z1)+z2.*conj(z2)+0.5*z3.*conj(z3))...
        +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2)...
        +3/2*beta3*z2.*z2.*conj(z3)+3/2*beta3*z3.*z3.*conj(z2)...
        +3/2*beta3*z1.*z1.*conj(z3)...
        +noise*sqrt(10^((pow_Noise_dB)/10));


%     y_LNAout = beta1*(z1+z2)...
%         +3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2))...
%         +3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2))...
%         +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2)...
%         +noise*sqrt(10^((pow_Noise_dB)/10));
%     
    temp = filter(bhi,1,y_LNAout.*conj(carrier1));
    z1_tilde_filter = temp(101:61e2).*carrier1(1:6e3);
    
    temp = filter(bhi,1,y_LNAout.*conj(carrier2));
    z2_tilde_filter = temp(101:61e2).*carrier2(1:6e3);
    
    temp = filter(bhi,1,y_LNAout.*conj(carrier3));
    z3_tilde_filter = temp(101:61e2).*carrier3(1:6e3);
%     z0_tilde_filter = filter(bhi,1,y_LNAout);
    %%
%     z1_tilde_theo = beta1*z1+beta3*z1.*(abs(z1).^2+abs(z2).^2);
%     z2_tilde_theo = beta1*z2+beta3*z2.*(abs(z1).^2+abs(z2).^2);
%     
%     figure
%     plot(abs((z2_tilde_theo(1:5e2))),'b');hold on
%     plot(abs((z2_tilde_filter(1e2:6e2))),'r');hold on
%     figure
%     plot(20*log10(abs(fft(y_LNAout))/length(y_LNAout)),'r');hold on
%     
    %%
    
%     s_n0 = (abs(z1_tilde_filter).^2+abs(z2_tilde_filter).^2)*3*beta3;
    s_n = (abs(z1_tilde_filter).^2)*3*beta3/2;

    k0 = zeros(6e3,1);
    k1 = zeros(6e3,1);
    k2 = zeros(6e3,1);
    k3 = zeros(6e3,1);
    
    for jj=1:6000
        temp = roots([1,-beta1,0,-s_n(jj)]);
        k1(jj) = real(temp(1));
%         temp = roots([1,-beta1,0,-s_n0(jj)]);
%         k0(jj) = real(temp(1));
    end
    
    k2 = beta1+3*beta3*(abs(z1_tilde_filter)./k1).^2;
    k3 = beta1+3*beta3*(abs(z1_tilde_filter)./k1).^2;
    
    z0_tilde_recons21 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
    z0_tilde_recons12 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);
    z0_tilde_recons23 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z3_tilde_filter./k3);
    z0_tilde_recons32 = 3/2*beta3*(z3_tilde_filter./k3).^2.*conj(z2_tilde_filter./k2);
    z0_tilde_recons13 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z3_tilde_filter./k3);

%     z0_tilde_recons21_close = 3*beta3/2*(z2_tilde_filter).^2.*conj(z1_tilde_filter)./k0.^3;
%     z0_tilde_recons12_close = 3*beta3/2*(z1_tilde_filter).^2.*conj(z2_tilde_filter)./k0.^3;
    
    %% theta from LMS filter
    vofn = 3*beta3/2*z1.*z1.*conj(z2);
    uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    % figure
    % plot(abs(vofn),'b');hold on
    % plot(abs(beta3*uofn),'r')
    %
    thetaLMS(1) = beta3;
    stepsize = 10e10;
    for ii=1:6000
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end
    
    z0_tilde_recons12_LMS = thetaLMS(1:6e3)'.*uofn;

    
    vofn = 3*beta3/2*z2.*z2.*conj(z1);
    uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
    % figure
    % plot(abs(vofn),'b');hold on
    % plot(abs(beta3*uofn),'r')
    %
    thetaLMS(1) = beta3;
    stepsize = 10e10;
    for ii=1:6000
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end
    
    z0_tilde_recons21_LMS = thetaLMS(1:6e3)'.*uofn;
    
    
    
    vofn = 3*beta3/2*z2.*z2.*conj(z3);
    uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
    % figure
    % plot(abs(vofn),'b');hold on
    % plot(abs(beta3*uofn),'r')
    %
    thetaLMS(1) = beta3;
    stepsize = 10e10;
    for ii=1:6000
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end
    
    z0_tilde_recons23_LMS = thetaLMS(1:6e3)'.*uofn;

    
    vofn = 3*beta3/2*z3.*z3.*conj(z2);
    uofn = 3/2*(z3_tilde_filter.*z3_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    % figure
    % plot(abs(vofn),'b');hold on
    % plot(abs(beta3*uofn),'r')
    %
    thetaLMS(1) = beta3;
    stepsize = 10e10;
    for ii=1:6000
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end
    
    z0_tilde_recons32_LMS = thetaLMS(1:6e3)'.*uofn;

    
    vofn = 3*beta3/2*z1.*z1.*conj(z3);
    uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
    % figure
    % plot(abs(vofn),'b');hold on
    % plot(abs(beta3*uofn),'r')
    %
    thetaLMS(1) = beta3;
    stepsize = 10e10;
    for ii=1:6000
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end
    
    z0_tilde_recons13_LMS = thetaLMS(1:6e3)'.*uofn;

   
    %%
%     k_true = beta1+beta3*(abs(z1).^2+abs(z2).^2);
%     figure;
%     subplot(211)
%     plot(k_true)
%     hold on
%     plot(k(100:end))
%     subplot(212)
%     plot(10*log10(abs(k_true(1:1e3)-k(101:1100))))
    %%
    
%     y_Fdomain = (abs(fft(z0_tilde_recons))/length(z0_tilde_recons)).^2;
%     %
%     figure
%     plot(10*log10(y_Fdomain))
    %%
%     k_true = beta1+beta3*(abs(z1).^2+abs(z2).^2);
%     orig_term = z0_tilde_filter(101:61e2);
%     cancel_term32 = beta3*z0_tilde_recons32./(k(1:60e2).^3);
%     cancel_term23 = beta3*z0_tilde_recons23./(k(1:60e2).^3);
%     cancel_term13 = beta3*z0_tilde_recons13./(k(1:60e2).^3);
    
    cancel_term = z0_tilde_recons21+z0_tilde_recons12+z0_tilde_recons23+z0_tilde_recons32+z0_tilde_recons13;
%     cancel_term_close = z0_tilde_recons21_close+z0_tilde_recons12_close;
    cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS+z0_tilde_recons32_LMS+z0_tilde_recons23_LMS+z0_tilde_recons13_LMS;
%     cancel_term = cancel_term21+cancel_term12+cancel_term23+cancel_term32+cancel_term13;
%     figure
%     plot(abs(orig_term));hold on
%     plot(abs(cancel_term1))
    
%     figure
%     plot(20*log10(abs(fft(orig_term))/6e3));hold on
%     plot(20*log10(abs(fft(orig_term-cancel_term1-cancel_term2))/6e3),'r');hold on
%     grid on
%     
%     figure
%     temp = 20*log10(abs(fft(orig_term-cancel_term1)/6e3));
%     plot([temp(3001:end);temp(1:3000)],'r');hold on
%     temp = 20*log10(abs(fft(cancel_term2))/6e3);
%     plot([temp(3001:end);temp(1:3000)],'g');hold on

    %%
%     k_true = beta1+beta3*(abs(z1).^2+abs(z2).^2);
%     cancel_term = beta3*z0_tilde_recons(101:61e2)./(k(101:61e2).^3);
%     cancel_term = beta3*z0_tilde_recons(101:61e2)./(k_true(1:60e2).^3);
    
    figure
    subplot(221)
    y_uncomp_Fdomain = 20*log10(abs(fft(y_LNAout(1:6e3).*hann(6e3))/6e3));
    plot([y_uncomp_Fdomain(3001:end);y_uncomp_Fdomain(1:3000)])
    grid on
    ylim([-140,-20])
    title('non-compensated')
    
    
    subplot(222)
    y_comp_Fdomain = 20*log10(abs(fft((y_LNAout(1:6e3)-cancel_term).*hann(6e3)))/6e3);
    plot([y_comp_Fdomain(3001:end);y_comp_Fdomain(1:3000)])
    grid on
    ylim([-140,-20])
    title('strongest-first')
    
%     subplot(223)
%     y_comp_Fdomain = 20*log10(abs(fft((y_LNAout(1:6e3)-cancel_term_close).*hann(6e3)))/6e3);
%     plot([y_comp_Fdomain(3001:end);y_comp_Fdomain(1:3000)])
%     grid on
%     ylim([-140,-20])
%     title('ignoring of 0.5/1 term')
    
    subplot(224)
    y_comp_Fdomain = 20*log10(abs(fft((y_LNAout(1:6e3)-cancel_term_LMS).*hann(6e3)))/6e3);
    plot([y_comp_Fdomain(3001:end);y_comp_Fdomain(1:3000)])
    grid on
    ylim([-140,-20])
    title('LMS theta')
    
    
    
%%
test_num = 1e3;
z_abs = linspace(1,2e4,test_num);
for jj=1:test_num
    temp = roots([1,-beta1,0,-z_abs(jj)]);
    k_test(jj) = real(temp(1));
end
% figure;plot(k_test)
% figure;plot(beta3./(k_test.^3).*beta1^3)
figure
plot(10+10*log10(-4.*abs(beta1)./3./(beta3.*(k_test./beta1).^3)))
%%
figure;
plot(abs(cancel_term21(1:6e3)),'b--');hold on
plot(abs(3/2*beta3*z2.*z2.*conj(z1)),'r-.')

figure;
plot(abs(cancel_term12(1:6e3)),'b--');hold on
plot(abs(3/2*beta3*z1.*z1.*conj(z2)),'r-.')
%%
figure;
% plot(abs(z1),'b--');hold on
plot(abs(z1(1:6e3)-z1_tilde_filter./beta1),'b');hold on
plot(abs(z1(1:6e3)-z1_tilde_filter./k1),'r')
%%
figure;
% plot(abs(z1),'b--');hold on
plot(abs(z1(1:6e3)-z1_tilde_filter./beta1),'b');hold on
plot(abs(z1(1:6e3)-z1_tilde_filter./k0),'g');hold on
plot(abs(z1(1:6e3)-z1_tilde_filter./k1),'r')
%%
figure
plot(abs(z1_tilde_filter)./abs(z1(1:6e3)));hold on
plot(abs(z2_tilde_filter)./abs(z2(1:6e3)));
ylim([0,60])
%%
figure;
plot(abs(z),'b');hold on
plot(abs(z1_base),'r');hold on
%%
% beta3_eq = beta3*(beta1./(beta1+3*beta3*(1/2*abs(z1.*conj(carrier1)).^2+abs(z2.*conj(carrier2)).^2))).^2.*(beta1./(beta1+3*beta3*(1/2*abs(z2.*conj(carrier2)).^2+abs(z1.*conj(carrier1)).^2)));
% theat_mymethod = beta3*beta1^3./k1.^2./k2;
% figure(99)
% plot(10+10*log10(-4*beta1/3./beta3_eq),'b')
% hold on
% plot(10+10*log10(-4*beta1/3./theat_mymethod),'r')

%
% figure(99);
% plot(10+10*log10(-4*beta1/3./thetaLMS),'g--','linewidth',1)

%%
%     figure
%     plot(abs(z1_tilde(1:1200)))

%%
%     CMD = beta3*z2.*z2.*conj(z1)+noise0*sqrt(pow_noise);
% 
%     z0_tilde = z2_tilde.*z2_tilde.*conj(z1_tilde);
%     
%     
%     s_n = (abs(z1_tilde).^2+abs(z2_tilde).^2)*beta3;
%     k = zeros(sig_length,1);
%     for jj=1:sig_length
%         temp = roots([1,-beta1,0,-s_n(jj)]);
%         k(jj) = real(temp(1));
%     end
%     
%     error_noadpt(runindex) = norm(CMD - beta3*z0_tilde./(k.^3))^2/sig_length;
%     for ii=1:theta_num
%         theta = theta_pool(ii);
%         error_adpt(runindex,ii) = norm(CMD - theta*z0_tilde./(beta1.^3))^2/sig_length;
% %         error_noncomepen(ii) = norm(CMD)^2/sig_length;
%     end
%     
% end
%%
% MSE_adpt = mean(error_adpt,1);
% MSW_noadpt = mean(error_noadpt);
% figure;
% plot(IIP3_pool,10*log10(MSE_adpt));hold on
% plot(IIP3_pool,ones(length(IIP3_pool),1)*10*log10(MSW_noadpt))
% % plot(IIP3_pool,10*log10(error_noncomepen))

%%
% z0_est = z0_tilde./(k.^3);
% figure;
% plot(abs(z0_tilde(1:100))/beta1^3,'linewidth',3);hold on
% plot(abs(z0_est(1:100)),'linewidth',3);hold on
% plot(abs(CMD(1:100)/beta3),'linewidth',3)
% legend('CMD_tilde','CMD_est','CMD')