% 2018/11/15
% Test script for frequency and spatial IMD

%-------------------------------------
% Script control parameter
%-------------------------------------
clear; clc; warning off;
rand('seed',4)

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 8;
M = 10;                                   % signal length control
N = 125;                                  % signal length control
channel_num = 100;                        % normalized freq. (100MHz)
SampleNumberAve = 100;                    % signal length control
stat_num = 2e2;                           % signal length control
P = M*N*8;                                % signal length control
sig_length = M*SampleNumberAve*stat_num;  % Signal length for processing
FFT_size = 2^13;                          % FFT size
noise_pow_dBm = -80;                      % AWGN power in dBm
blk_freq(1) = 0;                          % Blocker 1's baseband freq offset in MHz
blk_freq(2) = 25;                         % Blocker 2's baseband freq offset in MHz
sig_pow_dBm(1) = -90;                     % Power of z1 in dBm
sig_pow_dBm(2) = -60;                     % Power of z2 in dBm
sig_phi(1) = 0/pi*180;                    % AoA of signal 1 in rad
sig_phi(2) = 30/pi*180;                   % AoA of blocker 2 in rad

Nr = 16;                                  % Antenna size in receiver

MCtimes = 2e2;                            % Monte Carlo Simulation

% ----------    LNA parameter    -------------
% in 28GHz, Gain is [8.5 30 12.8], IIP3_dBm is [5,-1.5,5] dBm
gain_dB = 35;                             % LNA linear gain in dB
IIP3_dBm = -10;                           % LNA 3rd order Input intercept point in dBm
[beta1,beta3] = get_beta(gain_dB,IIP3_dBm);   % LNA parameter


BLK_pow_num = 20;
BLK_pow_range = linspace(-65,-15,BLK_pow_num);
%-------------------------------------
% Generate Test Waveform (SC) in Baseband
%-------------------------------------
QAM_level = 4;
[ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, blk_num );

%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
[ OFDM_env_rx_norm ] = get_MC_waveform( P );
% WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
% bhi0 = fir1(200,0.09,'low');
% OFDMLike = filter(bhi0,1,WGNLike);
% OFDMLike = OFDMLike./norm(OFDMLike).*sqrt(length(OFDMLike));

% Zero initialization of matrix
SINR = zeros(BLK_pow_num,MCtimes);

for MCidx = 1:MCtimes
    clc;fprintf(['Iteration ' num2str(MCidx) '\n']);
for BLK_pow_idx = 1:BLK_pow_num
    
sig_pow_dBm(2) = BLK_pow_range(BLK_pow_idx);   
%% Blocker (BLK) Generation in Non-Zero Baseband

start_point = randi(sig_length-P-1,blk_num,1);
for ii=1:blk_num
    sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
end

sig_pow = 10.^((sig_pow_dBm-17.5)./10); % the fixed offset is due to 50 om resistance

% Baseband frequency shifting and amplitude scaling
[ z1 ] = get_BB_freq_shift( OFDM_env_rx_norm, blk_freq(1), sig_pow(1), channel_num);
[ z2 ] = get_BB_freq_shift( sig_cache(:,1)./norm(sig_cache(:,1))*sqrt(length(sig_cache(:,1))), blk_freq(2), sig_pow(2), channel_num);

%% LNA non-linearity model in each antenna

for kk=1:Nr
    y_LNAout_nocrop(:,kk) = get_nonlinear_output( beta1, beta3,...
                                z1*exp(1j*pi*(kk-1)*sin(sig_phi(1))),...
                                z2*exp(1j*pi*(kk-1)*sin(sig_phi(2))))...
                            + get_AWGN( P, 10^(noise_pow_dBm/10) );
                        
    y_LNAout(:,kk) = y_LNAout_nocrop(:,kk);
end

Rx_filter_angle = 0/180*pi; %sig_phi(1);
Rx_spatial_filter = conj(exp(1j * pi * sin(Rx_filter_angle) * (0:(Nr-1)).'));
y_PSout = y_LNAout * Rx_spatial_filter;

    % crop maximum to 1
%     for ii=1:P
%         if real(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=1 + 1j*imag(y_LNAout(ii));
%         end
%         if imag(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=imag(y_LNAout(ii)) + 1j;
%         end
%     end
    
%% Quick PSD watch

% [x_data, PSD] = get_PSD(y_LNAout, FFT_size);
% figure(88)
% plot(x_data,PSD,'b')
% grid on
% ylim([-180,-60])
% xlabel('Frequency (MHz)')
% ylabel('Power Density (dBm/Hz)')
% title('Non-compensated PSD')

%% Digital Filtering of Two Self-Distorted BLKs in Baseband
    
% bhi = fir1(200,0.2,'low');
% carrier = exp(1j*(blk_freq(1)/channel_num*2*pi*(0:P-1)'));
% temp = filter(bhi,1,y_LNAout.*conj(carrier));
% z1_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);
% 
% % bhi = fir1(200,0.22,'low');
% carrier = exp(1j*(blk_freq(2)/channel_num*2*pi*(0:P-1)'));
% temp = filter(bhi,1,y_LNAout.*conj(carrier));
% z2_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);
%     
%%
bhi = fir1(200,0.2,'low');
temp = filter(bhi,1,y_PSout);
s1_tilde = temp(101:FFT_size+100);
alpha_est = pinv(z1(1:FFT_size))*s1_tilde;

sig_est = alpha_est*z1(1:FFT_size);
sig_pow_est = norm(sig_est)^2;

distortion_est = s1_tilde - sig_est;
distortion_pow = norm(distortion_est)^2;

SINR(BLK_pow_idx,MCidx) = sig_pow_est/distortion_pow;
end
end


%%
figure
plot(BLK_pow_range,10*log10(mean(SINR,2)),'linewidth',2);hold on
xlabel('BLK Power [dBm]');
ylabel('SINR [dB]')
grid on


%% Figure plotting
figure

[x_freq, PSD] = get_PSD(y_PSout(1:FFT_size), FFT_size);
h = area(x_freq,-41+PSD,-180);hold on
h(1).FaceColor = 'b';
h(1).EdgeColor = 'b';
grid on
ylim([-180,-60])
xlabel('Frequency (MHz)')
ylabel('PSD (dBm/Hz)')
title('Non-compensated PSD')


