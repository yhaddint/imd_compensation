% 2018/11/22
% Test script for digital linearization of mmW Rx array

%-------------------------------------
% Script control parameter
%-------------------------------------
clear; clc; warning off;
rand('seed',4)

%-------------------------------------
% System Parameters
%-------------------------------------
UE_num = 8;                               % Total UE number in UL
UE_of_interest = 2;                       % index of UE that we evaluate
SampleNumberAve = 5;                      % signal length control
P = 1e4;                                  % signal length control
sig_length = 50*P;                        % Signal length for processing
FFT_size = 2^13;                          % FFT size
noise_pow_dBm = -85;                      % AWGN power in dBm

sig_pow_dBm = [-50,-75,-50,-50,-50,-50,-50,-50];          % Power of z1 in dBm
sig_phi = linspace(-60,60,UE_num)/180*pi;         % AoA of signal 1 in rad

Nr = 128;                                  % Antenna size in receiver

MCtimes = 100;                              % Monte Carlo Simulation

% ----------    LNA parameter    -------------
% in 28GHz, Gain is [8.5 30 12.8], IIP3_dBm is [5,-1.5,5] dBm
gain_dB = 15;                             % LNA linear gain in dB
IIP3_dBm = -10;                           % LNA 3rd order Input intercept point in dBm

IP3_num = 11;
IP3_range = linspace(-30,-0,IP3_num);
%-------------------------------------
% Generate Test Waveform (SC) in Baseband
%-------------------------------------
QAM_level = 16;
[ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, UE_num );

%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
[ OFDM_env_rx_norm ] = get_MC_waveform( P );
% WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
% bhi0 = fir1(200,0.09,'low');
% OFDMLike = filter(bhi0,1,WGNLike);
% OFDMLike = OFDMLike./norm(OFDMLike).*sqrt(length(OFDMLike));

% Zero initialization of matrix
SINR = zeros(IP3_num,MCtimes);

for MCidx = 1:MCtimes
    clc;fprintf('Iteration %d out of %d\n', MCidx, MCtimes);
    for IP3_idx = 1:IP3_num
    
    % LNA parameters
    IIP3_dBm = IP3_range(IP3_idx);
    [beta1,beta3_neg] = get_beta(gain_dB,IIP3_dBm);   % LNA parameter
    beta3 = -beta3_neg;

    % ------- Signal Waveform (BLK) Generation in Non-Zero Baseband ---------

    start_point = randi(sig_length-P-1,UE_num,1);
    sig_pow = 10.^((sig_pow_dBm-17.5)./10); % the fixed offset is due to 50 om resistance
    for ii=1:UE_num
        sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(UE_num));
        sig_UE(:,ii) = sqrt(sig_pow(ii)) * sig_cache(:,1)./norm(sig_cache(:,1))*sqrt(length(sig_cache(:,1)));
    end

    % ------ LNA non-linearity model in each antenna -----------
    
    for kk=1:Nr
        sig_in = zeros(length(sig_UE(:,ii)),1);
        for ii=1:UE_num
            sig_in = sig_in + sig_UE(:,ii) * exp(1j*pi*(kk-1)*sin(sig_phi(ii)));
        end
        y_LNAout(:,kk) = beta1 * sig_in + beta3 * sig_in .* abs(sig_in).^2 ...
                        + get_AWGN( P, 10^(noise_pow_dBm/10) );
    end
    
    % ------ ADC Quantization Model -----------
    
    for kk=1:Nr
        max_mag = max(max(abs(real(y_LNAout(:,kk)))),max(abs(imag(y_LNAout(:,kk)))));
        ADC_bits = 6;
        y_ADCout(:,kk) = DAC_quan( y_LNAout(:,kk), ADC_bits, max_mag );
    end
    

    % ------ Spatial Matched Filter (MIMO Detection) -----------
    Rx_filter_angle = sig_phi(UE_of_interest);
    for ii=1:UE_num
        H_MIMO(:,ii) = exp(1j * pi * sin(sig_phi(ii)) * (0:(Nr-1)).');
    end
%     Rx_spatial_filter = conj(exp(1j * pi * sin(Rx_filter_angle) * (0:(Nr-1)).'));
    RBF_ZF = pinv(H_MIMO);
    RBF_ZF_normal = RBF_ZF./norm(RBF_ZF,'fro');
    Rx_spatial_filter = RBF_ZF_normal(2,:).';
    y_PSout = y_ADCout * Rx_spatial_filter;

    
    % ----- Compensation Algorithm
    sig_square = sum(abs(y_LNAout * RBF_ZF_normal.').^2,2);
    comp_weight = pinv([sig_UE(1:P,UE_of_interest), y_PSout.*sig_square])*y_PSout;
    sig_post_comp = y_PSout - comp_weight(2)*(y_PSout.*sig_square);
    
    alpha_est_comp = pinv(sig_UE(1:FFT_size,UE_of_interest))*sig_post_comp(1:FFT_size);

    sig_est_comp = alpha_est_comp*sig_UE(1:FFT_size,UE_of_interest);
    sig_pow_est_comp = norm(sig_est_comp)^2;

    distortion_est_comp = sig_post_comp(1:FFT_size) - sig_est_comp;
    distortion_pow_comp = norm(distortion_est_comp)^2;
    
    SINR_comp(IP3_idx,MCidx) = sig_pow_est_comp/distortion_pow_comp;
    
        % crop maximum to 1
    %     for ii=1:P
    %         if real(y_LNAout_nocrop(ii))>1
    %             y_LNAout(ii)=1 + 1j*imag(y_LNAout(ii));
    %         end
    %         if imag(y_LNAout_nocrop(ii))>1
    %             y_LNAout(ii)=imag(y_LNAout(ii)) + 1j;
    %         end
    %     end

    % ------ Quick PSD watch ----------

    % [x_data, PSD] = get_PSD(y_LNAout, FFT_size);
    % figure(88)
    % plot(x_data,PSD,'b')
    % grid on
    % ylim([-180,-60])
    % xlabel('Frequency (MHz)')
    % ylabel('Power Density (dBm/Hz)')
    % title('Non-compensated PSD')

    % --------- Digital Filtering of Signal in Baseband ---------

    % bhi = fir1(200,0.2,'low');
    % carrier = exp(1j*(blk_freq(1)/channel_num*2*pi*(0:P-1)'));
    % temp = filter(bhi,1,y_LNAout.*conj(carrier));
    % z1_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);
    % 
   
    %%

    alpha_est = pinv(sig_UE(1:FFT_size,UE_of_interest))*y_PSout(1:FFT_size);

    sig_est = alpha_est*sig_UE(1:FFT_size,UE_of_interest);
    sig_pow_est = norm(sig_est)^2;

    distortion_est = y_PSout(1:FFT_size) - sig_est;
    distortion_pow = norm(distortion_est)^2;
    
    SINR(IP3_idx,MCidx) = sig_pow_est/distortion_pow;

    end
end

%%
figure
plot(IP3_range,10*log10(mean(SINR,2)),'-x','linewidth',2);hold on
plot(IP3_range,10*log10(mean(SINR_comp,2)),'--o','linewidth',2);hold on

xlabel('IIP3 [dBm]');
ylabel('Eff. Stream SINR [dB]')
grid on
legend('no comp','comp')


%% Figure plotting
% figure
% 
% [x_freq, PSD] = get_PSD(y_PSout(1:FFT_size), FFT_size);
% h = area(x_freq,-41+PSD,-180);hold on
% h(1).FaceColor = 'b';
% h(1).EdgeColor = 'b';
% grid on
% ylim([-180,-60])
% xlabel('Frequency (MHz)')
% ylabel('PSD (dBm/Hz)')
% title('Non-compensated PSD')


