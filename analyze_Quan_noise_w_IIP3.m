%-------------------------------------
% Script control parameter
%-------------------------------------
clear; clc; warning off;
rand('seed',3)

%-------------------------------------
% System Parameters
%-------------------------------------
SampleNumberAve = 5;                      % signal length control
P = 5e3;                                  % signal length control
sig_length = 50*P;                        % Signal length for processing
FFT_size = 2^13;                          % FFT size
UE_num = 8;                               % Total UE number in UL
UE_of_interest = 2;                       % index of UE that we evaluate
Nr = 64;                                  % antenna size
% sig_pow_dBm = -45;                      % Power of z1 in dBm
sig_pow_dBm = ones(1,UE_num)*(-51);
sig_pow_dBm(2) = -70;                     % Power of z1 in dBm
sig_phi = [-35,-25,-15,-5,5,15,25,35]'/180*pi;               % AoA of signal 1 in rad

MCtimes = 1;                            % Monte Carlo Simulation

% ----------    LNA parameter    -------------
% in 28GHz, Gain is [8.5 30 12.8], IIP3_dBm is [5,-1.5,5] dBm
gain_dB = 15;                             % LNA linear gain in dB
% IIP3_dBm = -10;                           % LNA 3rd order Input intercept point in dBm

IP3_num = 13;
IP3_range = linspace(-40,-16,IP3_num);    % LNA 3rd order Input intercept point in dBm


%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
% [ OFDM_env_rx_norm ] = get_MC_waveform( P );
ADC_bits_range = 6:8;
dist_stream_normalized = zeros(length(ADC_bits_range),IP3_num);
dist_stream_normalized2 = zeros(length(ADC_bits_range),IP3_num);
dist_stream_BM = zeros(length(ADC_bits_range),IP3_num);
dist_stream_Nocomp = zeros(length(ADC_bits_range),IP3_num);


for ip3_idx = 1:IP3_num
% ----------- LNA parameters -------------------
IIP3_dBm = IP3_range(ip3_idx);
[beta1,beta3_neg] = get_beta(gain_dB,IIP3_dBm);   % LNA parameter
beta3 = -beta3_neg;


for ii=1:UE_num
    sig_mag = dBm2V(sig_pow_dBm(ii));
    sig_UE_raw(:,ii) = randn(P,1) + 1j*randn(P,1);
    sig_UE(:,ii) = sig_mag * sig_UE_raw(:,ii)./norm(sig_UE_raw(:,ii))*sqrt(P);
end

% ------ Input Signal of LNA -----------
spatial_res = exp(1j*pi*sin(sig_phi)*(0:(Nr-1))); % row - UE index, col - antenna index
sig_in = sig_UE * spatial_res;


% ------ LNA non-linearity model in each antenna -----------
y_LNAout = beta1 * sig_in + beta3 * sig_in .* abs(sig_in).^2;
y_LNAout_BM = beta1 * sig_in;



%% 
beta_range = linspace(0.5,1,10); % loading fraction range

for bits_idx = 1:length(ADC_bits_range)
    
    ADC_bits = ADC_bits_range(bits_idx);
    fprintf('IP3 = %d dBm, ADC Quan = %d Bits\n',IIP3_dBm, ADC_bits);
    
    for kk = 1 : Nr
        
        % ------ ADC Quantization Model -----------
        max_mag(kk) = max(max(abs(real(y_LNAout(:,kk)))),max(abs(imag(y_LNAout(:,kk)))));

        for beta_idx = 1:10
%         y_ADCout(:,kk) = DAC_quan( y_LNAout(:,kk), ADC_bits, max_mag(kk) );
            max_ADC_V = beta_range(beta_idx) * max_mag(kk);
            y_ADCout_beta(:,beta_idx) = ADC_overload( y_LNAout(:,kk), ADC_bits, max_ADC_V );
            Quan_Pow(beta_idx) = norm(y_ADCout_beta(:,beta_idx) - y_LNAout(:,kk));
        end
        [~,beta_best] = min(Quan_Pow);
        y_ADCout(:,kk) = y_ADCout_beta(:,beta_best);

        
        max_mag_BM(:,kk) = max(max(abs(real(y_LNAout_BM(:,kk)))),max(abs(imag(y_LNAout_BM(:,kk)))));
        
        for beta_idx = 1:10
            max_ADC_V = beta_range(beta_idx) * max_mag_BM(kk);
            y_ADCout_BM_beta(:,beta_idx) = ADC_overload( y_LNAout_BM(:,kk), ADC_bits, max_ADC_V );
            Quan_Pow_BM(beta_idx) = norm(y_ADCout_BM_beta(:,beta_idx) - y_LNAout_BM(:,kk));
        end
        [~,beta_best_BM] = min(Quan_Pow_BM);
        y_ADCout_BM(:,kk) = y_ADCout_BM_beta(:,beta_best_BM);
%         y_ADCout_BM(:,kk) = DAC_quan( y_LNAout_BM(:,kk), ADC_bits, max_mag_BM(:,kk) );

        % ------ Compensation Design (Look-Up Table) ---------
        LUT = [abs(sig_in(:,kk)).^2, abs(y_LNAout(:,kk)).^2];

        % ------ Compensation Design (1st/3rd order Filtering) ---------
        XDATA = [y_ADCout(:,kk), y_ADCout.*abs(y_ADCout(:,kk)).^2];
        comp_weight = pinv(XDATA) * (y_ADCout_BM(:,kk)/beta1);%sig_in;
        y_post_comp2(:,kk) = XDATA*comp_weight;

        % ----- Compensation Execution ---------
        y_post_comp(:,kk) = zeros(P,1);
        for pp=1:P
            [~,idx] = min(abs(abs(y_ADCout(pp,kk))^2 - LUT(:,2)));
    %         close all
    %         figure;plot(LUT(:,1),LUT(:,2),'.');hold on;grid on;
    %         plot(LUT(idx,1),LUT(idx,2),'o','linewidth',2)
            y_post_comp(pp,kk) = y_ADCout(pp,kk) / sqrt(LUT(idx,2)) * sqrt(LUT(idx,1));
        end
    end
    
    % ------ MIMO detection ----------
    Rx_filter_angle = sig_phi(UE_of_interest);
    for ii = 1 : UE_num
        H_MIMO(:,ii) = exp(1j * pi * sin(sig_phi(ii)) * (0:(Nr-1)).');
    end
    RBF_ZF = pinv(H_MIMO);
    RBF_ZF_normal = RBF_ZF./norm(RBF_ZF,'fro');
    Rx_spatial_filter = RBF_ZF_normal(UE_of_interest,:).';
    y_MIMOout_comp = y_post_comp * Rx_spatial_filter;
    y_MIMOout_comp2 = y_post_comp2 * Rx_spatial_filter;
    y_MIMOout_BM = y_ADCout_BM * Rx_spatial_filter;
    y_MIMOout_nocomp = y_ADCout * Rx_spatial_filter;
    
    
%     if ADC_bits==4
%         
%         figure
%         h1 = plot(real(y_post_comp),imag(y_post_comp),'o');hold on
%         h2 = plot(real(y_post_comp2),imag(y_post_comp2),'s');hold on
%         h3 = plot(real(y_ADCout_BM/beta1),imag(y_ADCout_BM/beta1),'x');hold on
%         legend('NL, LUT Comp.','NL, 1st/3rd Filter.','Linear')
%         grid on
%         set([h1,h2,h3],'linewidth',2);
%         set([h1,h2,h3],'markersize',8);
%         set(gca,'FontSize',14)
% 
%     end
    
%     subplot(212)
%     plot(real(y_ADCout/beta1),imag(y_ADCout/beta1),'.');hold on
%     title('Pre-Comp')
%     grid on
%     legend('Post Comp.','Ideal')
    %
    % figure
    % % plot((real(y_LNAout)),(real(y_ADCout)),'r.');hold on
    % %
    % % plot(abs(real(sig_in)),beta1*abs(real(sig_in)),'k.');hold on
    % % plot(abs(imag(sig_in)),beta1*abs(imag(sig_in)),'k.');hold on
    % plot((real(sig_in)),(real(y_post_comp)),'r.');hold on
    % plot((imag(sig_in)),(imag(y_post_comp)),'r.');hold on
    % grid on
    % xlabel('Input Level [mV]')
    % ylabel('Output Level [mV]')
    %
    % ----- Evaluation (Pre-MIMO-detection)---------
    ant_of_interest = 2;
    alpha_comp = pinv(sig_in(:,ant_of_interest))*y_post_comp(:,ant_of_interest);
    dist_ant_normalized(bits_idx) = norm(y_post_comp(:,ant_of_interest) -...
        alpha_comp*sig_in(:,ant_of_interest))^2/norm(alpha_comp*sig_in(:,ant_of_interest))^2;
    
    alpha_comp2 = pinv(sig_in(:,ant_of_interest))*y_post_comp2(:,ant_of_interest);
    dist_ant_normalized2(bits_idx) = norm(y_post_comp2(:,ant_of_interest) -...
        alpha_comp2*sig_in(:,ant_of_interest))^2/norm(alpha_comp2*sig_in(:,ant_of_interest))^2;
    
    
    alpha_BM = pinv(sig_in(:,ant_of_interest))*(y_ADCout_BM(:,ant_of_interest)/beta1);
    dist_ant_BM(bits_idx) = norm((y_ADCout_BM(:,ant_of_interest)/beta1) -...
        alpha_BM*sig_in(:,ant_of_interest))^2/norm(alpha_BM*sig_in(:,ant_of_interest))^2;
    
    alpha_nocomp = pinv(sig_in(:,ant_of_interest))*(y_ADCout(:,ant_of_interest));
    dist_ant_Nocomp(bits_idx) = norm(y_ADCout(:,ant_of_interest) -...
        alpha_nocomp*sig_in(:,ant_of_interest))^2/norm(alpha_nocomp*sig_in(:,ant_of_interest))^2;

    
    
    
    % ----- Evaluation (Post-MIMO-detection)---------
    alpha_comp = pinv(sig_UE(:,UE_of_interest))*y_MIMOout_comp;
    dist_stream_normalized(bits_idx,ip3_idx) = norm(y_MIMOout_comp - alpha_comp*sig_UE(:,UE_of_interest))^2/norm(alpha_comp*sig_UE(:,UE_of_interest))^2;
    
    alpha_comp2 = pinv(sig_UE(:,UE_of_interest))*y_MIMOout_comp2;
    dist_stream_normalized2(bits_idx,ip3_idx) = norm(y_MIMOout_comp2 - alpha_comp2*sig_UE(:,UE_of_interest))^2/norm(alpha_comp2*sig_UE(:,UE_of_interest))^2;
    
    
    alpha_BM = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_BM/beta1);
    dist_stream_BM(bits_idx,ip3_idx) = norm((y_MIMOout_BM/beta1) - alpha_BM*sig_UE(:,UE_of_interest))^2/norm(alpha_BM*sig_UE(:,UE_of_interest))^2;
    
    alpha_nocomp = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_nocomp);
    dist_stream_Nocomp(bits_idx,ip3_idx) = norm(y_MIMOout_nocomp - alpha_nocomp*sig_UE(:,UE_of_interest))^2/norm(alpha_nocomp*sig_UE(:,UE_of_interest))^2;

end
end


%% Performance v.s. ADC quantization level
% figure
% % h1 = plot(ADC_bits_range,10*log10(dist_stream_normalized),'-o');
% % hold on
% h2 = plot(ADC_bits_range,10*log10(dist_stream_normalized2),'-^');
% hold on
% h3 = plot(ADC_bits_range,10*log10(dist_stream_Nocomp),'-.s');
% hold on
% h4 = plot(ADC_bits_range,10*log10(dist_stream_BM),'--x');
% hold on
% title('Stream-Level Distortion')
% set([h2,h3,h4],'linewidth',2);
% set([h2,h3,h4],'markersize',8);
% 
% set(gca,'FontSize',14)
% 
% grid on
% xlabel('ADC Bits')
% ylabel('Normalized Distortion [dB]')
% legend('NL, Prop. Comp.','NL, No Comp.','Linear')
%% Performance v.s. IIP3 specification
figure
h2 = plot(IP3_range,10*log10(dist_stream_normalized(3,:)),'-^');
hold on
h3 = plot(IP3_range,10*log10(dist_stream_Nocomp(3,:)),'-.s');
hold on
% h4 = plot(IP3_range,10*log10(dist_stream_BM),'--x');
% hold on
title('Stream-Level Distortion')
set([h2,h3],'linewidth',2);
set([h2,h3],'markersize',8);
set(gca,'FontSize',14)
grid on
ylim([-20,10])
xlim([-40,-16])
xlabel('LNA IP3 [dBm]')
ylabel('Normalized Distortion [dB]')
legend('NL, Prop. Comp.','NL, No Comp.')
xticks(linspace(-40,-16,7))



