% 04/23/2016
% spectrum sensing performance with IMD3
% it uses SINR and number of samples as criterion
% 
clear;clc;
%%
open('SOI_-100dBm.fig')
h = findobj(gca,'Type','line')
x=get(h,'Xdata')
y=get(h,'Ydata')
close all
%%
% axesObjs = get(h, 'Children');  %axes handles
% dataObjs = get(axesObjs, 'Children');
% objTypes = get(dataObjs, 'Type');
% 
% xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
% ydata = get(dataObjs, 'YData');
% % zdata = get(dataObjs, 'ZData');
% close all
%%
sir_num = 16;
sir_range = zeros(5,16);
sir_range(1,:) = y{1};
sir_range(2,:) = y{2};
sir_range(3,:) = y{3};
sir_range(4,:) = y{4};
sir_range(5,:) = y{5};
sir_range(6,:) = y{6};
sir_range(7,:) = y{7};
%%
blk_range = x{1};

x_data = [-22, -21, -20, -19];
x_data_left = [-40,-39,-38];

temp(1,:) = [24.1, 16.81, -9.194, -32.43];
temp(2,:) = [-17.49, -32.73, -47.45, -57.61];
temp(3,:) = [-63.5, -66.54, -69.53, -72.58];
temp(4,:) = [-46.34, -51.55, -60.61,-72.82];
temp(5,:) = [-38.74, -46.76, -54.39,-80.10];
temp(6,:) = [-24.65, -28.36, -32.05,-34.93];
temp(7,:) = [-18.06, -21.88, -26.13,-31.08];
sir_range_left = ones(7,3)*60;
sir_range_left(3,:) = [-9,-12,-15];
sir_range_extend = [sir_range_left,sir_range,temp];
blk_range_extend = [x_data_left,blk_range-103,x_data];
%%
figure;plot(blk_range_extend,sir_range_extend','linewidth',3)
%%
sample_num = 500;
for caseindex = 1:7
    for sirindex = 1:sir_num+7
        sir = sir_range_extend(caseindex,sirindex);
        sinr = 1/(10^(-sir/10)+1);
        pd(sirindex,caseindex) = qfunc(qfuncinv(0.05)-sqrt(sinr*sample_num));
    end
end
figure;
plot(blk_range_extend+3,pd,'linewidth',3);hold on
grid on
legend('no-comp','1','2','3','4','5','6')

    