% 04/12/2016
% how ADC clipping noise introduce distortion in spectrum sensing
clear;clc;clf;close all

rand('seed',4)
blk_num = 8;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8;
L = M*SampleNumberAve*stat_num;
FFT_size = 2^11;


for ii = 1:blk_num
%     sig_r(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig_i(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig(:,ii) = (sig_r(:,ii)+sig_i(:,ii))/sqrt(2);
    upsam = SampleNumberAve;
    symbols = fix(L*2/upsam);   
    clearvars data temp_data
    hmod = modem.qammod('M', 4, 'InputType', 'integer');
    hdesign  = fdesign.pulseshaping(upsam,'Square Root Raised Cosine');
    hpulse = design(hdesign);
    data = randi(4,symbols,1)-1;
    data = modulate(hmod, data);
    data = upsample(data,upsam);
    temp_data = conv(data,hpulse.Numerator);
    sig(:,ii) = temp_data(end-L+1:end)./sqrt(temp_data(end-L+1:end)'*temp_data(end-L+1:end)/L);
end


%% BLK generating
    
    
    start_point = randi(L-P-1,blk_num,1);
    for ii=1:blk_num
        sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
    end
    pow_dB = ones(8,3)*(-10);
    pow_dB(:,2) = pow_dB(:,1)-15;
%     pow_dB(:,3) = pow_dB(:,1)-10-3;
    onoff = rand(8,3);
%     pow_dB = rand(8,3)*20-40;
    pow = (10.^((pow_dB-17.5)./10)).*(onoff>0);
    
    f1 = -12.5;
    f2 = 12;
%     f3 = 30;
    
    PhaseShift1 = exp(1j*rand*2*pi);
    PhaseShift2 = exp(1j*rand*2*pi);
%     PhaseShift3 = exp(1j*rand*2*pi);
    
    carrier1 = exp(1j*(f1/channel_num*2*pi*(1:P)'));
    carrier2 = exp(1j*(f2/channel_num*2*pi*(1:P)'));
%     carrier3 = exp(1j*(f3/channel_num*2*pi*(1:P)'));
    
    WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
    bhi0 = fir1(200,0.09,'low');
%     freqz(bhi0)
    OFDMLike = filter(bhi0,1,WGNLike);
%     z1_base = kron(sqrt(pow(:,1)),ones(M*N,1)).*OFDMLike;
%     z2_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*sig_cache(:,2);
%     z3_base = kron(sqrt(pow(:,3)),ones(M*N,1)).*sig_cache(:,3);

    z1_base = kron(sqrt(pow(:,1)),hann(M*N)).*OFDMLike;
    z2_base = kron(sqrt(pow(:,2)),hann(M*N)).*sig_cache(:,2);
%     z3_base = kron(sqrt(pow(:,3)),hann(M*N)).*sig_cache(:,3);
    
    z1 = z1_base.*carrier1*PhaseShift1;
    z2 = z2_base.*carrier2*PhaseShift2;
%     z3 = z3_base.*carrier3*PhaseShift3;
%% AWGN
    sig_length = length(z1);
    noise = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
    pow_Noise_dB = -90;
%% clipping noise
    beta1 = 56.23;
    y_LNAout = beta1*(z2)+noise*sqrt(10^((pow_Noise_dB)/10));
    y_ADCout = zeros(FFT_size,1);
    ADC_th = 0.5;
    record_real = zeros(FFT_size,1);
    for ii=1:FFT_size
        % real part quantization
        if real(y_LNAout(ii))>ADC_th
            bit_real = ADC_th;
            record_real(ii)=1;
        elseif real(y_LNAout(ii))<-ADC_th
            bit_real = -ADC_th;
            record_real(ii)=-1;
        else
            bit_real = real(y_LNAout(ii));
        end
        % imag part quantization
%         if imag(y_LNAout(ii))>ADC_th
%            bit_imag = ADC_th;
%         elseif imag(y_LNAout(ii))<-ADC_th
%             bit_imag = -ADC_th;
%         else
%             bit_imag = imag(y_LNAout(ii));
%         end
        bit_imag = imag(y_LNAout(ii));
        
        y_ADCout(ii) = bit_real+1j*bit_imag;
    end
    %%
    figure;
    plot(abs(y_LNAout));hold on
    plot(abs(y_ADCout),'r');hold on
    %% quick PSD watch

temp = 20*log10(abs(fft(y_LNAout(1:FFT_size).*hann(FFT_size)))/FFT_size);
figure;
plot(temp)
figure
plot(linspace(0,100,FFT_size),[temp(FFT_size/2+1:end);temp(1:FFT_size/2)],'r')
    %%
    clip_noise = y_ADCout-y_LNAout(1:FFT_size);
    figure
    plot(clip_noise,'x')
    %%
    temp = 20*log10(abs(fft(clip_noise(1:FFT_size).*hann(FFT_size)))/FFT_size);
    figure
    plot(linspace(0,100,FFT_size),[temp(FFT_size/2+1:end);temp(1:FFT_size/2)],'r')
    
    %% least square creterion
    y = fft(y_ADCout(1:FFT_size));
    A = fft(record_real);
    w = [ones(632,1);zeros(800,1);ones(6760,1)];
    x = (dftmtx(FFT_size)*(w.*fft(record_real)));
    
    