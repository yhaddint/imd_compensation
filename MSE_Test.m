% 04/23/2016
% complimentary curve for the version 3: 
%    y axis: SINR of SOI 
%    x axis: interferers to noise ratio INR
%    theoretical aprx of LMS, emprical value of LMS, emprical of
%    non-compensated are plotted
% SIR v.s. power of interferers with proposed compensation method
% 
clear;clc;%clf;close all

rand('seed',3)
blk_num = 15;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8*5;
L = M*SampleNumberAve*stat_num;
FFT_size = 2^13;



for ii = 1:blk_num
%     sig_r(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig_i(:,ii) = kron(randi(2,M*stat_num,1)*2-3,ones(SampleNumberAve,1));
%     sig(:,ii) = (sig_r(:,ii)+sig_i(:,ii))/sqrt(2);
    upsam = SampleNumberAve;
    symbols = fix(L*2/upsam);   
    clearvars data temp_data
    hmod = modem.qammod('M', 4, 'InputType', 'integer');
    hdesign  = fdesign.pulseshaping(upsam,'Square Root Raised Cosine');
    hpulse = design(hdesign);
    data = randi(4,symbols,1)-1;
    data = modulate(hmod, data);
    data = upsample(data,upsam);
    temp_data = conv(data,hpulse.Numerator);
    sig(:,ii) = temp_data(end-L+1:end)./sqrt(temp_data(end-L+1:end)'*temp_data(end-L+1:end)/L);
end

% LNA non-linearity model
beta1 = 56.23;
beta3 = -7497.33;

%
 z3powdB = -100-17.5;
%%

% BLKpow_range = linspace(-37,-19,19);
BLKpow_range = [-35,-30,-25,-20]-3;
% BLKpow_range = linspace(-22,-19,4);
% BLKpow_range = -22;

BLKpow_num = length(BLKpow_range);
runtimes = 10;

MSE_theo = zeros(1,BLKpow_num);
MSE_empr = zeros(runtimes,BLKpow_num,40);
MSE_nocomp = zeros(runtimes,BLKpow_num);

ite = zeros(runtimes,BLKpow_num,P);

%%
ite_num = 10;
for BLKpowindex=1:BLKpow_num
    BLKpowindex

    z3pow = 10^(z3powdB/10);

    for runindex=1:runtimes
        runindex
        start_point = randi(L-P-1,blk_num,1);
        for ii=1:2
            sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
        end
        
        pow_dB = ones(1,2)*BLKpow_range(BLKpowindex);
        pow = 10.^((pow_dB-17.5)./10);
        
        
        WGNLike = (randn(P,1)+1j*randn(P,1))./sqrt(2);
        bhi0 = fir1(200,0.1,'low');
        OFDMLike = filter(bhi0,1,WGNLike);
        OFDMLike = OFDMLike./norm(OFDMLike)*sqrt(P);
        
        z1_base = sqrt(pow(:,1)).*OFDMLike;
        z2_base = sqrt(pow(:,2)).*sig_cache(:,2);
        
        z1 = z1_base;
        z2 = z2_base;
        
        noise1 = (randn(P,1)+1j*randn(P,1))*sqrt((1e-10)/2);
        noise2 = (randn(P,1)+1j*randn(P,1))*sqrt((1e-10)/2);
        
        z1_tilde = z1.*(beta1+3/2*beta3.*(abs(z1).^2+2*abs(z2).^2))+noise1;
        z2_tilde = z2.*(beta1+3/2*beta3.*(abs(z2).^2+2*abs(z1).^2))+noise2;
        
        z1_tilde_env = abs(z1_tilde);
        z2_tilde_env = abs(z2_tilde);
        
        dofn = 3*beta3/2*conj(z1).*z2.^2;

        k1 = zeros(ite_num,P);
        k2 = zeros(ite_num,P);
        for jj=1:P
            z1_est = zeros(ite_num,1);
            z2_est = zeros(ite_num,1);
            temp = roots([1,-beta1,0,-(abs(z1_tilde_env(jj)).^2)*3*beta3/2]);
            z1_est(1) = z1_tilde_env(jj)/temp(1);
            temp = roots([1,-beta1,0,-(abs(z2_tilde_env(jj)).^2)*3*beta3/2]);
            z2_est(1) = z2_tilde_env(jj)/temp(1);

            for ii=1:ite_num
                if ii>=2
                    z1_est(ii) = z1_est(ii-1)*(1+z1_tilde_error(ii-1));
                    z2_est(ii) = z2_est(ii-1)*(1+z2_tilde_error(ii-1));
                end
                z1_tilde_est = z1_est(ii)*(beta1+3*beta3*(z1_est(ii)^2/2+z2_est(ii)^2));
                z1_tilde_error(ii) = (z1_tilde_env(jj)-z1_tilde_est)/z1_tilde_env(jj);
                z2_tilde_est = z2_est(ii)*(beta1+3*beta3*(z1_est(ii)^2+z2_est(ii)^2/2));
                z2_tilde_error(ii) = (z2_tilde_env(jj)-z2_tilde_est)/z2_tilde_env(jj);
                k1(ii,jj) = z1_tilde_env(jj)/z1_est(ii);
                k2(ii,jj) = z2_tilde_env(jj)/z2_est(ii);
            end
        end
        for ii=1:ite_num
            z0_tilde_recons21(:,ii) = ...
                3/2*beta3*(z2_tilde./k2(ii,:)').^2.*conj(z1_tilde./k1(ii,:)');
            error_empr_proposed = dofn-z0_tilde_recons21(:,ii);
            MSE_empr_proposed(runindex,BLKpowindex,ii) = mean(abs(error_empr_proposed).^2); 
        end
    end
end
%%
for BLKpowindex=1:BLKpow_num
    for ii=1:ite_num
        MSE_empr_proposed_cuml(BLKpowindex,ii) = 10*log10(min(mean(squeeze(MSE_empr_proposed(:,BLKpowindex,ii)),1)));
    end
end


figure
plot(1:ite_num,(z3powdB+17.5-MSE_empr_proposed_cuml)','linewidth',3);hold on
grid on
% legend('LMS (aprx.)','LMS (sim.)','w/o compensation')
xlabel('Interation Number')
ylabel('SINR (dB)')
legend('-35 dBm Interferers','-30 dBm Interferers','-25 dBm Interferers','-20 dBm Interferers')
%

