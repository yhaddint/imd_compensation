function [beta1,beta3] = get_beta(gain_dB,IIP3_dBm)
%GET_BETA Summary of this function goes here
%   Detailed explanation goes here

beta1 = sqrt(10^(gain_dB/10));
VIP3 = 10^((IIP3_dBm-10)/20);
beta3 = 4/3*abs(beta1)/(VIP3^2);


end

