function [ y_out ] = get_nonlinear_output( beta1, beta3, z1, z2, z3, z4 )
%GET_NONLINEAR_BB_OUTPUT Summary of this function goes here
%   Detailed explanation goes here

    switch nargin
        case 6
            y_out = beta1*(z1+z2+z3+z4)...
                    +3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3)+z4.*conj(z4))...
                    +3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2)+z3.*conj(z3)+z4.*conj(z4))...
                    +3*beta3*z3.*(z1.*conj(z1)+z2.*conj(z2)+0.5*z3.*conj(z3)+z4.*conj(z4))...
                    +3*beta3*z4.*(z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3)+0.5*z4.*conj(z4))...
                    +3/2*beta3*z1.*z1.*conj(z2)... % 2f1-f2
                    +3/2*beta3*z1.*z1.*conj(z3)... % 2f1-f3
                    +3/2*beta3*z1.*z1.*conj(z4)... % 2f1-f4
                    +3/2*beta3*z2.*z2.*conj(z1)... % 2f2-f1
                    +3/2*beta3*z2.*z2.*conj(z3)... % 2f2-f3
                    +3/2*beta3*z2.*z2.*conj(z4)... % 2f2-f4
                    +3/2*beta3*z3.*z3.*conj(z1)... % 2f3-f1
                    +3/2*beta3*z3.*z3.*conj(z2)... % 2f3-f2
                    +3/2*beta3*z3.*z3.*conj(z4)... % 2f3-f4
                    +3/2*beta3*z4.*z4.*conj(z1)... % 2f4-f1
                    +3/2*beta3*z4.*z4.*conj(z2)... % 2f4-f2
                    +3/2*beta3*z4.*z4.*conj(z3);   % 2f4-f3
        case 5
            y_out = beta1*(z1+z2+z3)...
                    +3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3))...
                    +3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2)+z3.*conj(z3))...
                    +3*beta3*z3.*(z1.*conj(z1)+z2.*conj(z2)+0.5*z3.*conj(z3))...
                    +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2)...
                    +3/2*beta3*z2.*z2.*conj(z3)+3/2*beta3*z3.*z3.*conj(z2)...
                    +3/2*beta3*z1.*z1.*conj(z3);
        case 4
            y_out = beta1*(z1+z2)...
                    +3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2))...
                    +3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2))...
                    +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2);
        otherwise
            fprintf('Only support up to 4 inputs\n');
    end
end

