function [thetaLMS] = IMD_LMS(vofn,uofn,stepsize,startpoint,FFT_size)
%CMD_LMS_V1 Summary of this function goes here
%   Detailed explanation goes here
    thetaLMS(1) = startpoint;
    for ii=1:FFT_size-1
        thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
    end

end
