function [z1_scale,z2_scale] = blk_reconstruct(beta1,beta3,z1_tilde_env,z2_tilde_env)
%BLOCK_ESTIMATION_V1 uses fixed number of iteration
%   Detailed explanation goes here

%     z1_tilde_true = z1_tilde_env;
%     z2_tilde_true = z2_tilde_env;
    ite_num = 5;
    z1_est = zeros(ite_num,1);
    z2_est = zeros(ite_num,1);
    temp = roots([1,-beta1,0,-(abs(z1_tilde_env).^2)*3*beta3/2]);
    z1_est(1) = z1_tilde_env/temp(1);
    temp = roots([1,-beta1,0,-(abs(z2_tilde_env).^2)*3*beta3/2]);
    z2_est(1) = z2_tilde_env/temp(1);

    for ii=1:ite_num
        if ii>=2
            z1_est(ii) = z1_est(ii-1)*(1+z1_tilde_error(ii-1));
            z2_est(ii) = z2_est(ii-1)*(1+z2_tilde_error(ii-1));
        end
        z1_tilde_est = z1_est(ii)*(beta1+3*beta3*(z1_est(ii)^2/2+z2_est(ii)^2));
        z1_tilde_error(ii) = (z1_tilde_env-z1_tilde_est)/z1_tilde_env;
        z2_tilde_est = z2_est(ii)*(beta1+3*beta3*(z1_est(ii)^2+z2_est(ii)^2/2));
        z2_tilde_error(ii) = (z2_tilde_env-z2_tilde_est)/z2_tilde_env;
    end
    
    z1_scale = z1_tilde_env/z1_est(ite_num);
    z2_scale = z2_tilde_env/z2_est(ite_num);
end