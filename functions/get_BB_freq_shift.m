function [ z_out ] = get_BB_freq_shift( z_sig, BBfreq, blk_pow, channel_num)
%GET_BB_FREQ_SHIFT Summary of this function goes here
%   Detailed explanation goes here

sig_length = length(z_sig);
PhaseShift = exp(1j*rand*2*pi); % Blockers have unknown phase offset
carrier = exp(1j*(BBfreq/channel_num*2*pi*(0:sig_length-1)')); % Blockers baseband freq shift BBfreq
z_out = sqrt(blk_pow)* PhaseShift * z_sig .* carrier; % Blockers z1 after BB freq shift


end

