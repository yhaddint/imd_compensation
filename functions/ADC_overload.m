function [s_out_complx] = ADC_overload(input_complx, bits_num, maxamp)
%ADC_QUAN_OVERLOAD Summary of this function goes here
%   Detailed explanation goes here
    epsilon = 1e-9; % term to round correctly
    
    temp_scale = 2^(bits_num-1);
    
    s_in_real = real(input_complx);
    s_in_imag = imag(input_complx);
    
    s_in_real_clip = s_in_real;
    s_in_real_clip(find(s_in_real >= maxamp)) = maxamp-epsilon;
    s_in_real_clip(find(s_in_real <= -maxamp)) = -maxamp+epsilon;
    
    s_in_imag_clip = s_in_imag;
    s_in_imag_clip(find(s_in_imag >= maxamp)) = maxamp-epsilon;
    s_in_imag_clip(find(s_in_imag <= -maxamp)) = -maxamp+epsilon;

    
    s_real_raw = round(s_in_real_clip/maxamp * temp_scale+0.5);
    s_imag_raw = round(s_in_imag_clip/maxamp * temp_scale+0.5);
       
    s_out_real = (s_real_raw - (0.5)) / temp_scale * maxamp;
    s_out_imag = (s_imag_raw - (0.5)) / temp_scale * maxamp;
    
    s_out_complx = s_out_real + 1j * s_out_imag;


end

