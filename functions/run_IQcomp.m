function [ x_hat ] = run_IQcomp (y, gm, phim)
%RUN_IQCOMP Summary of this function goes here
%   Detailed explanation goes here
    
    x_hat = zeros(length(y),1);
    A = [1,0;-gm*sin(phim),gm*cos(phim)];
    B = inv(A);
    for ii=1:length(y)
        xiq = (B*[real(y(ii));imag(y(ii))]).';
        x_hat(ii) = xiq(1) + 1j* xiq(2);
    end

end

