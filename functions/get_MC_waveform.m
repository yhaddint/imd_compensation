function [ OFDM_env_rx_norm ] = get_MC_waveform( P )
%GET_MC_WAVEFORM Summary of this function goes here
%   Detailed explanation goes here


OFDMcarrier = 1024;
for ii=1:floor(P/10/OFDMcarrier)+1
    OFDMsymb = (randi(2,OFDMcarrier,1)*2-3)+1j*(randi(2,OFDMcarrier,1)*2-3);
    OFDMsamp = ifft(OFDMsymb);
    OFDM_env((ii-1)*OFDMcarrier+1:ii*OFDMcarrier,1) = OFDMsamp;
end
OFDM_env_oversample = kron(OFDM_env(1:P/10),ones(10,1));
bhi = fir1(200,0.09,'low');
OFDM_env_rx = filter(bhi,1,OFDM_env_oversample);
OFDM_env_rx_norm = OFDM_env_rx./norm(OFDM_env_rx).*sqrt(length(OFDM_env_rx));



end

