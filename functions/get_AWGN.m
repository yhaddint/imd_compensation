function [ awgn ] = get_AWGN( sig_length, awgn_pow )
%GET_AWGN Summary of this function goes here
%   Detailed explanation goes here

noise_unitpow = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
awgn = noise_unitpow * sqrt(awgn_pow);

end

