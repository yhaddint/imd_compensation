function [ sig_out ] = get_clipping( sig_in, clip_level)
%GET_CLIPPING Summary of this function goes here
%   Detailed explanation goes here

sig_out = sig_in;
for kk=1:length(sig_in)
    mag = abs(sig_in(kk));
    if mag>clip_level
        sig_out(kk) = sig_in(kk)/mag*clip_level;
    end
end

end

