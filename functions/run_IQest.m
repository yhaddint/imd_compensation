function [ gm_hat, phim_hat ] = run_IQest( y, N_IQest )
%RUN_IQEST Summary of this function goes here
%   Estimate parameter in IQ imbalance blindly
%   It requires independent I/Q data with zero mean for best accuracy
%   [ gm, phim ] = run_IQest( y, N_IQest )
%   IP: y, input vector with IQ imbalance
%   IP: N_IQest, scaler specify num sample to estimate parameter
%   OP: gm, estimated gain mismatch parameter
%   OP: phim, estimated phase mismatch parameter

    sig = y(1:N_IQest);
    sig_i = real(sig);
    sig_q = imag(sig);

    
    % I/Q channel power
    sigma2hat = sum(sig_i.^2);
    
    % step1: estimate gm
    gm_hat = sqrt((sum(sig_q.^2))/sigma2hat);
    
    % step2: estimate beta
    beta_hat = -sum(sig_i.*sig_q)/sigma2hat;
    
    % step3, estimate phim, it uses asin(x) = x for small x
    phim_hat = beta_hat * (2 - gm_hat);

end

