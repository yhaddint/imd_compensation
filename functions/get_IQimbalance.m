function [ y ] = get_IQimbalance( x, gm, phim )
%GET_IQIMBALANCE Summary of this function goes here
%   It generates IQ imbalance based on [grimm14] paper
%   Relationship in complex signal (x complex input, y complex output)
%        y = k1 x + k2 conj(x),
%   where k1 = (1+gm*exp(-1j*phim))/2; k2 = (1-gm*exp(1j*phim))/2;
%   Alternative relationship in I/Q channel
%   [Re(y); Im(y)] = [1, 0; -gm*sin(phim), gm*cos(phim)] * [Re(x);Im(x)]
%
%   Function definition
%   [ sig_out ] = get_IQimbalance( sig_in, gm, phim )
%   IP: sig_in, N by 1 complex vector of input signal
%   IP: gm, gain mismatch coefficient between I and Q channel
%   IP: phim, phase mismatch between I and Q channel
%   OP: sig_out, N by 1 complex vector of output signal

    y_IQ_i = real(x);
    y_IQ_q = gm*cos(phim)*imag(x) -gm*sin(phim)*real(x);
    y = y_IQ_i + 1j * y_IQ_q;

end

