clear;

SOIpowrange = -129:3:-90;
% SOIpowrange = -90;
for SOIpowindex = 1:length(SOIpowrange)
    SOIpow = SOIpowrange(SOIpowindex);
    filename = ['y_LNAout',num2str(round(-SOIpow)),'.mat'];
    load(filename)
    temp = sort(powBOI_nocomp,'descend');
    TH_nocomp = (temp(90)+temp(91))/2;
    temp = sort(powBOI_prop,'descend');
    TH_prop = (temp(90)+temp(91))/2;
    
    filename = 'y_LNAout200.mat';
    load(filename)
    pd_ws_nocomp(SOIpowindex) = sum(powBOI_nocomp<TH_nocomp)/100;
    pd_ws_prop(SOIpowindex) = sum(powBOI_prop<TH_prop)/100;
    
end

%%
figure
plot(SOIpowrange,pd_ws_nocomp,'linewidth',2);hold on
plot(SOIpowrange,pd_ws_prop,'linewidth',2);hold on
xlabel('Power of SOI')
ylabel('Detection Prob.')