% 2018/01/24
% Main script

%-------------------------------------
% Script control parameter
%-------------------------------------
clear;clc;close all
rand('seed',4)

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 8;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8;
sig_length = M*SampleNumberAve*stat_num;
FFT_size = 2^13;
noise_pow_dBm = -90; % AWGN power in dBm
blk_freq(1) = -12.5; % Blocker 1 has equivalent baseband freq -12.5MHz
blk_freq(2) = 12.5; % Blocker 2 has equivalent baseband freq 12.5MHz
sig_pow_dBm(1) = -30; % Power of z1 in dBm
sig_pow_dBm(2) = -30; % Power of z2 in dBm

%-------------------------------------
% Generate Test Waveform (SC) in Baseband
%-------------------------------------
QAM_level = 4;
[ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, blk_num );

%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
[ OFDM_env_rx_norm ] = get_MC_waveform( P );
% WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
% bhi0 = fir1(200,0.09,'low');
% OFDMLike = filter(bhi0,1,WGNLike);
% OFDMLike = OFDMLike./norm(OFDMLike).*sqrt(length(OFDMLike));

%% Blocker (BLK) Generation in Non-Zero Baseband

start_point = randi(sig_length-P-1,blk_num,1);
for ii=1:blk_num
    sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
end

sig_pow = 10.^((sig_pow_dBm-17.5)./10); % the fixed offset is due to 50 om resistance

% Baseband frequency shifting and amplitude scaling
[ z1 ] = get_BB_freq_shift( sig_cache(:,1), blk_freq(1), sig_pow(1), channel_num);
[ z2 ] = get_BB_freq_shift( OFDM_env_rx_norm, blk_freq(2), sig_pow(2), channel_num);

%% LNA non-linearity model
    
beta1 = 56.23;
beta3 = -7497.33;

% Be careful about signal cropping.
% In fixed point operation, it has trouble if magnitude is greater than 1.
y_LNAout_nocrop = get_nonlinear_output( beta1, beta3, z1, z2)...
                    + get_AWGN( P, 10^(noise_pow_dBm/10) );
y_LNAout = y_LNAout_nocrop;

    % crop maximum to 1
%     for ii=1:P
%         if real(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=1 + 1j*imag(y_LNAout(ii));
%         end
%         if imag(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=imag(y_LNAout(ii)) + 1j;
%         end
%     end
    
%% Quick PSD watch

% [x_data, PSD] = get_PSD(y_LNAout, FFT_size);
% figure(88)
% plot(x_data,PSD,'b')
% grid on
% ylim([-180,-60])
% xlabel('Frequency (MHz)')
% ylabel('Power Density (dBm/Hz)')
% title('Non-compensated PSD')

%% Digital Filtering of Two Self-Distorted BLKs in Baseband
    
bhi = fir1(200,0.2,'low');
carrier = exp(1j*(blk_freq(1)/channel_num*2*pi*(0:P-1)'));
temp = filter(bhi,1,y_LNAout.*conj(carrier));
z1_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);

% bhi = fir1(200,0.22,'low');
carrier = exp(1j*(blk_freq(2)/channel_num*2*pi*(0:P-1)'));
temp = filter(bhi,1,y_LNAout.*conj(carrier));
z2_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);
    
%% The proposed approach

s_n = (abs(z1_tilde_filter).^2)*3*beta3/2;

k0 = zeros(FFT_size,1);
k1 = zeros(FFT_size,1);
k2 = zeros(FFT_size,1);

for jj=1:FFT_size
    [k1(jj),k2(jj)] = blk_reconstruct(beta1,beta3,abs(z1_tilde_filter(jj)),abs(z2_tilde_filter(jj)));
end

z0_tilde_recons1 = (z1_tilde_filter./k1).*(3/2*beta3*abs(z1_tilde_filter./k1).^2+3*beta3*abs(z2_tilde_filter./k2).^2);
z0_tilde_recons2 = (z2_tilde_filter./k2).*(3/2*beta3*abs(z2_tilde_filter./k2).^2+3*beta3*abs(z1_tilde_filter./k1).^2);
z0_tilde_recons21 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
z0_tilde_recons12 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);

%% theta from LMS filter
%     stepsize = 6e10;
stepsize = 1e10;
LMS_ite_num = 200;

% LMS for IMD z1 * z1 * conj(z2)
vofn = 3*beta3/2*z1.*z1.*conj(z2);
uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z0_tilde_recons12_LMS = thetaLMS(end)'*uofn;

% LMS for IMD z2 * z2 * conj(z1)
vofn = 3*beta3/2*z2.*z2.*conj(z1);
uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z0_tilde_recons21_LMS = thetaLMS(end)'.*uofn;

% LMS for IMD z1 * z1 * conj(z1)
vofn = 3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2));
uofn = 3/2*z1_tilde_filter.*(z1_tilde_filter.*conj(z1_tilde_filter)+...
        2*z2_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z_tilde_recons11_LMS = thetaLMS(end)'.*uofn;

% LMS for CMD z2 * z2 * conj(z2)
vofn = 3*beta3*z2.*(0.5*z2.*conj(z2)+z1.*conj(z1));
uofn = 3/2*z2_tilde_filter.*(z2_tilde_filter.*conj(z2_tilde_filter)+...
        2*z1_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z_tilde_recons22_LMS = thetaLMS(end)'.*uofn;

cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS+...
                    z_tilde_recons11_LMS+z_tilde_recons22_LMS;

%% Figure plotting
figure

subplot(211)
[x_freq, PSD] = get_PSD(y_LNAout(1:FFT_size), FFT_size);
h = area(x_freq,-41+PSD,-180);hold on
h(1).FaceColor = 'b';
h(1).EdgeColor = 'b';
grid on
ylim([-180,-60])
xlabel('Frequency (MHz)')
ylabel('PSD (dBm/Hz)')
title('Non-compensated PSD')

%
subplot(212)
waveform = y_LNAout(1:FFT_size)-cancel_term_LMS(1:FFT_size);
[x_freq, PSD] = get_PSD(waveform(1:FFT_size), FFT_size);
h = area(x_freq,-41+PSD,-180);hold on
h(1).FaceColor = 'g';
h(1).EdgeColor = 'g';

waveform = y_LNAout(1:FFT_size) - cancel_term(1:FFT_size);
[x_freq, PSD] = get_PSD(waveform(1:FFT_size), FFT_size);
h = area(x_freq,PSD-41,-180,'FaceColor','r');hold on
h(1).FaceColor = 'r';
h(1).EdgeColor = 'r';

grid on
ylim([-180,-60])
xlabel('Frequency (MHz)')
ylabel('PSD (dBm/Hz)')
legend('LMS','Proposed Method')
title('Compensated PSD') 




