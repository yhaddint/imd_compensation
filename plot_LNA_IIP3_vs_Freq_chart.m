clear;clc

% Data from AD website. freq correponds to lowest freq supported
freq=  [0.5	0.6	0.7	0.7	0.7	1	1	1.2	1.7	1.8	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	2	3.5	3.5	4.8	5	5	5	6	6	6	6	6	7	7	7	7.5	9	12	12	13	13	14	14	14	17	17	17	18	18	18	20	20	21	22	24	24	24	24	24	24	24	24	24	27	28	29	35	37	50	57	71	71	71];
iip3 = [3.5	2	6	6	6	1	2	1	-1	1	1	6.5	0.5	-2	5	-2	-2	0	1.5	0.5	0.5	0.5	11	11	5.5	3	4	2	1	-0.5	2	1.8	3.5	-3	-4	3	-12	-3.5	-3	-11	-12	-4	-5	-6	-1	-7	-2	-1	-14	-15	-3	-6	-4	-12	-7	-9	3	-3.4	-1	-7	-3	-4.5	-13	-8	-12	-6	-14	1	3.5	-7	-12	-17	-8	-9	-9	-6	-10	0	-9	-8	-6	-7];

figure

semilogx(freq,iip3,'o','linewidth',2);grid on

% Set the remaining axes properties
set(gca,'FontSize',14,'XMinorTick','on','XScale','log','XTick',[0.3 1 3 10 30 100],...
    'XTickLabel',{'0.3','1','3','10','30','100'});
xlabel('Freq [GHz]')
ylabel('P_i_n for 1dB Compression [dBm]')
xlim([0.3,100])