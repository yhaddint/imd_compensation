% 04/24/2017
% Spectrum sensing performance evaluation using energy detection and
% predefined threshold in
% 1) Ideal LNA 
% 2) Nonlinear LNA w/o compensation
% 3) Nonlinear LNA w/ LMS compensation (algorithm 1)
% 4) Nonlinear LNA w/ proposed compensation (algorithm 2)


%-------------------------------------
% Script control parameter
%-------------------------------------
clear;clc;clf;close all
rand('seed',3)
SOI_present = 1;

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 3;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8;
L = M*SampleNumberAve*stat_num;
FFT_size = 2^13; % 8196-FFT for evaluation
MCtimes = 10;

% LNA non-linearity model
beta1 = 56.23;
beta3 = -7497.33;

y_LNAout_noSOI = zeros(P, MCtimes);
SOIpowrange = [-200, -135:3:-90];
for SOIindex = 1:length(SOIpowrange)
    SOIpow = SOIpowrange(SOIindex);
    fprintf('SOI Power:    %4.2f dB\n', SOIpow);
    for MCindex = 1:MCtimes
        fprintf('Monte Carlo Iteration:    %4.2f\n', MCindex);

        % Generate Test Waveform (SC) in Baseband
        QAM_level = 4;
        [ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, blk_num );
       
        % Generate Test Waveform (MC) in Baseband
        [ OFDM_env_rx_norm ] = get_MC_waveform( P );
    
        % BLK generating
        start_point = randi(L-P-1,blk_num,1);
        for ii=1:blk_num
            sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
        end
    %     pow_dBm = ones(8,3)*(-25);
        pow_dBm(:,1) = ones(8,1)*(-35);
        pow_dBm(:,2) = ones(8,1)*(-35);
        pow_dBm(:,3) = ones(8,1)*(SOIpow);
        pow_dBm(:,4) = ones(8,1)*(SOIpow);
        pow = 10.^((pow_dBm-30)./10);
        if ~SOI_present
            pow(:,3) = 0;
            pow(:,4) = 0;
        end
        ampl = sqrt(pow*2*50);

        f1 = -12.5;
        f2 = 12.5;
        f3 = -17.5;
        f4 = -37.5;

        PhaseShift1 = exp(1j*rand*2*pi);
        PhaseShift2 = exp(1j*rand*2*pi);
        PhaseShift3 = exp(1j*rand*2*pi);
        PhaseShift4 = exp(1j*rand*2*pi);

        carrier1 = exp(1j*(f1/channel_num*2*pi*(1:P)'));
        carrier2 = exp(1j*(f2/channel_num*2*pi*(1:P)'));
        carrier3 = exp(1j*(f3/channel_num*2*pi*(1:P)'));
        carrier4 = exp(1j*(f4/channel_num*2*pi*(1:P)'));

        WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
        bhi0 = fir1(200,0.09,'low');
    %     freqz(bhi0)
        OFDMLike = filter(bhi0,1,WGNLike);
        OFDMLike = OFDMLike./norm(OFDMLike).*sqrt(length(OFDMLike));
        z1_base = kron(ampl(:,1),ones(M*N,1)).*sig_cache(:,1);
    %     z2_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*sig_cache(:,2);
        z2_base = kron(ampl(:,2),ones(M*N,1)).*OFDM_env_rx_norm;
        z3_base = kron(ampl(:,3),ones(M*N,1)).*sig_cache(:,2);
        z4_base = kron(ampl(:,4),ones(M*N,1)).*sig_cache(:,2);


        z1 = z1_base.*carrier1*PhaseShift1;
        z2 = z2_base.*carrier2*PhaseShift2;
        z3 = z3_base.*carrier3*PhaseShift3;
        z4 = z4_base.*carrier4*PhaseShift4;
        
        % AWGN
        sig_length = length(z1);
        noise = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
        pow_Noise_dB = -90;
        pow_noise = 10^((pow_Noise_dB-30)/10);
        noise_ampl = sqrt(pow_noise*2*50);
    
        % LNA non-linearity model     
        y_LNAout = get_nonlinear_output( beta1, beta3, z1, z2, z3, z4)...
                    +noise*noise_ampl;
       y_LNAout_all(:,MCindex) = y_LNAout;
    %    y_LNAout = y_LNAout/2;


    % activate two or three BLKs

    bhi = fir1(200,0.2,'low');
    temp = filter(bhi,1,y_LNAout.*conj(carrier1));
    z1_tilde_filter = temp(101:FFT_size+100).*carrier1(1:FFT_size);

    % bhi = fir1(200,0.22,'low');
    temp = filter(bhi,1,y_LNAout.*conj(carrier2));
    z2_tilde_filter = temp(101:FFT_size+100).*carrier2(1:FFT_size);



        s_n = (abs(z1_tilde_filter).^2)*3*beta3/2;

        k0 = zeros(FFT_size,1);
        k1 = zeros(FFT_size,1);
        k2 = zeros(FFT_size,1);
    %     k3 = zeros(FFT_size,1);

        for jj=1:FFT_size
            [k1(jj),k2(jj)] = blk_reconstruct(beta1,beta3,abs(z1_tilde_filter(jj)),abs(z2_tilde_filter(jj)));
        end


    %     k3 = beta1+3*beta3*(abs(z1_tilde_filter)./k1).^2;

        z0_tilde_recons1 = (z1_tilde_filter./k1).*(3/2*beta3*abs(z1_tilde_filter./k1).^2+3*beta3*abs(z2_tilde_filter./k2).^2);
        z0_tilde_recons2 = (z2_tilde_filter./k2).*(3/2*beta3*abs(z2_tilde_filter./k2).^2+3*beta3*abs(z1_tilde_filter./k1).^2);
        z0_tilde_recons21 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
        z0_tilde_recons12 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);

        %% theta from LMS filter

    %     stepsize = 6e10;
        stepsize = 1e10;
        LMS_ite_num = 1000;

        % LMS for CMD 112
        vofn = 3*beta3/2*z1.*z1.*conj(z2);
        uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
        thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
        z0_tilde_recons12_LMS = thetaLMS(end)'*uofn;

        % LMS for CMD 221
        vofn = 3*beta3/2*z2.*z2.*conj(z1);
        uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
        thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
        z0_tilde_recons21_LMS = thetaLMS(end)'.*uofn;


        cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
    %     cancel_term_close = z0_tilde_recons21_close+z0_tilde_recons12_close;
        cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS;%+...
            %z_tilde_recons11_LMS+z_tilde_recons22_LMS;


    %% figure plotting
        ss_index = linspace(-50,50,FFT_size);
        SOI_index = find(and((ss_index>(-12.5-1)), (ss_index<(-12.5+1))));
        plot_scale = [-50,50,-120,10];
        figure
        subplot(311)
        y_uncomp_Fdomain = 20*log10(abs(fft(y_LNAout(1:FFT_size).*hann(FFT_size))/FFT_size));
        
        % The total power reduces by 2.7 withhann window;
        % additional 10dB is used to convert volt into dBm in R=50om impedance
        % 81.92 is used to convert dBm/bin into dBm/MHz
        ss_uncomp = 10*log10(27)+10*log10(81.92)...
            +[y_uncomp_Fdomain(FFT_size/2+1:end);y_uncomp_Fdomain(1:FFT_size/2)];
        plot(ss_index, ss_uncomp);hold on
        plot(ss_index(SOI_index), ss_uncomp(SOI_index));hold on
        grid on
        axis(plot_scale);
        xlabel('Frequency (MHz)')
        ylabel('PSD (dBm/MHz)')
        title('Non-compensated PSD')
%         
%     
%         subplot(312)
%         y_comp_Fdomain_prop = 20*log10(abs(fft((y_LNAout(1:FFT_size)-cancel_term(1:FFT_size)).*hann(FFT_size)))/FFT_size);
%         ss_prop = 10*log10(27)+10*log10(81.92)...
%             +[y_comp_Fdomain_prop(FFT_size/2+1:end);y_comp_Fdomain_prop(1:FFT_size/2)];
%         plot(ss_index,ss_prop);hold on
%         plot(ss_index(SOI_index),ss_prop(SOI_index));hold on
%         hold on
%         title('Proposed') 
%         xlabel('Frequency (MHz)')
%         ylabel('PSD (dBm/MHz)')
%         axis(plot_scale);
%         grid on
%         
%         subplot(313)
%         y_comp_Fdomain_LMS = 20*log10(abs(fft((y_LNAout(1:FFT_size)-cancel_term_LMS(1:FFT_size)).*hann(FFT_size)))/FFT_size);
%         ss_LMS = 10*log10(27)+10*log10(81.92)...
%             +[y_comp_Fdomain_LMS(FFT_size/2+1:end);y_comp_Fdomain_LMS(1:FFT_size/2)];
%         plot(ss_index,ss_LMS);hold on
%         plot(ss_index(SOI_index),ss_LMS(SOI_index))
%         hold on
%         grid on
%         axis(plot_scale);
%         xlabel('Frequency (MHz)')
%         ylabel('PSD (dBm/MHz)')
%         title('LMS') 
%         grid on

    %% locating SOI in sensing

    ss_index = linspace(-50,50,FFT_size);
    SOI_index = find(and((ss_index>(-12.5-1)), (ss_index<(-12.5+1))));

    % xxx = abs(fft(y_LNAout(1:FFT_size).*hann(FFT_size)))/FFT_size;
    xxx = abs(fft(y_LNAout(1:FFT_size).*hann(FFT_size)))/FFT_size;
    ss_nocomp = [xxx(FFT_size/2+1:end);xxx(1:FFT_size/2)];

    xxx = abs(fft((y_LNAout(1:FFT_size)-cancel_term(1:FFT_size)).*hann(FFT_size)))/FFT_size;
    ss_prop = [xxx(FFT_size/2+1:end);xxx(1:FFT_size/2)];

    xxx = abs(fft((y_LNAout(1:FFT_size)-cancel_term_LMS(1:FFT_size)).*hann(FFT_size)))/FFT_size;
    ss_LMS = [xxx(FFT_size/2+1:end);xxx(1:FFT_size/2)];

    % The total power reduces by 2.7 withhann window;
    % additional 10dB is used to convert volt into dBm in R=50om impedance
    powBOI_nocomp(MCindex) = 2.7e1*sum(ss_nocomp(SOI_index).^2);
    powBOI_prop(MCindex) = 2.7e1*sum(ss_prop(SOI_index).^2);
    powBOI_LMS(MCindex) = 2.7e1*sum(ss_LMS(SOI_index).^2);
    end
%     filename = ['y_LNAout',num2str(round(-SOIpow)),'.mat'];
%     save(filename,'powBOI_nocomp','powBOI_prop','powBOI_LMS','y_LNAout_all');
end
