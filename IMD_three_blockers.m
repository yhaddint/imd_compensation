% 04/11/2016
% the self-interference of blocker is
% z1_tilde = z1*(beta1+3*beta3(0.5|z1|^2+|z2|^2))
% when power of z1 and z2 are significantly different, it is possible to
% estimate stronger one 

%-------------------------------------
% Script control parameter
%-------------------------------------
clear;clc;close all
rand('seed',4)

%-------------------------------------
% System Parameters
%-------------------------------------
blk_num = 8;
M = 10;
N = 125;
channel_num = 100;
SampleNumberAve = 100;
stat_num = 2e2;
P = M*N*8;
L = M*SampleNumberAve*stat_num;
FFT_size = 2^13;

%-------------------------------------
% Generate Test Waveform (SC) in Baseband
%-------------------------------------
QAM_level = 4;
[ sig ] = get_SC_waveform( QAM_level, sig_length, SampleNumberAve, blk_num )

%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
[ OFDM_env_rx_norm ] = get_MC_waveform( P )
% WGNLike = (randn(1e4,1)+1j*randn(1e4,1))./sqrt(2);
% bhi0 = fir1(200,0.09,'low');
% OFDMLike = filter(bhi0,1,WGNLike);
% OFDMLike = OFDMLike./norm(OFDMLike).*sqrt(length(OFDMLike));

%% BLK generating in non-zero Baseband


start_point = randi(L-P-1,blk_num,1);
for ii=1:blk_num
    sig_cache(:,ii) = sig(start_point(ii):start_point(ii)+P-1,randi(blk_num));
end
pow_dB = ones(8,3)*(-25);
pow_dB(:,1) = ones(8,1)*(-26);
pow_dB(:,2) = ones(8,1)*(-29);
%     pow_dB(:,3) = pow_dB(:,1)-3;
onoff = rand(8,3);
%     pow_dB = rand(8,3)*20-40;
%     pow = (10.^((pow_dB-17.5)./10)).*(onoff>0);
pow = 10.^((pow_dB-17.5)./10);

f1 = -12.5; % Blocker 1 has equivalent baseband freq -12.5MHz
f2 = 12.5; % Blocker 2 has equivalent baseband freq 12.5MHz
%     f3 = 30; % Blocker 3 has equivalent baseband freq 30MHz

PhaseShift1 = exp(1j*rand*2*pi); % Blockers have unknown phase offset
PhaseShift2 = exp(1j*rand*2*pi); % Blockers have unknown phase offset
%     PhaseShift3 = exp(1j*rand*2*pi); % Blockers have unknown phase offset

carrier1 = exp(1j*(f1/channel_num*2*pi*(1:P)')); % Blockers baseband freq shift f1
carrier2 = exp(1j*(f2/channel_num*2*pi*(1:P)')); % Blockers baseband freq shift f2
% carrier3 = exp(1j*(f3/channel_num*2*pi*(1:P)')); % Blockers baseband freq shift f3


z1_base = kron(sqrt(pow(:,1)),ones(M*N,1)).*sig_cache(:,1);
%     z2_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*sig_cache(:,2);
z2_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*OFDM_env_rx_norm;
%     z3_base = kron(sqrt(pow(:,2)),ones(M*N,1)).*OFDMLike;

%     z3_base = kron(sqrt(pow(:,3)),ones(M*N,1)).*sig_cache(:,3);

%     z1_base = kron(sqrt(pow(:,1)),hann(M*N)).*sig_cache(:,1);
%     z2_base = kron(sqrt(pow(:,2)),hann(M*N)).*sig_cache(:,2);
%     z3_base = kron(sqrt(pow(:,3)),hann(M*N)).*sig_cache(:,3);

z1 = z1_base.*carrier1*PhaseShift1;
z2 = z2_base.*carrier2*PhaseShift2;
%     z3 = z3_base.*carrier3*PhaseShift3;
%% AWGN
sig_length = length(z1);
noise = (randn(sig_length,1)+1j*randn(sig_length,1))/sqrt(2);
pow_Noise_dB = -90;
%% LNA non-linearity model
    
    beta1 = 56.23;
    beta3 = -7497.33;
    
%     y_LNAout = beta1*(z1+z2+z3)...
%         +beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3))...
%         +beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2)+z3.*conj(z3))...
%         +beta3*z3.*(z1.*conj(z1)+z2.*conj(z2)+0.5*z3.*conj(z3))...
%         +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2)...
%         +3/2*beta3*z2.*z2.*conj(z3)+3/2*beta3*z3.*z3.*conj(z2)...
%         +3/2*beta3*z1.*z1.*conj(z3)...
%         +noise*sqrt(10^((pow_Noise_dB)/10));

%         y_LNAout = beta1*(z1+z2+z3)...
%         +beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2)+z3.*conj(z3))...
%         +beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2)+z3.*conj(z3))...
%         +beta3*z3.*(z1.*conj(z1)+z2.*conj(z2)+0.5*z3.*conj(z3));

    y_LNAout_nocrop = beta1*(z1+z2)...
        +3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2))...
        +3*beta3*z2.*(z1.*conj(z1)+0.5*z2.*conj(z2))...
        +3/2*beta3*z2.*z2.*conj(z1)+3/2*beta3*z1.*z1.*conj(z2)...
        +noise*sqrt(10^((pow_Noise_dB)/10));
    y_LNAout = y_LNAout_nocrop;
    % crop maximum to 1
%     for ii=1:P
%         if real(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=1 + 1j*imag(y_LNAout(ii));
%         end
%         if imag(y_LNAout_nocrop(ii))>1
%             y_LNAout(ii)=imag(y_LNAout(ii)) + 1j;
%         end
%     end
    
    y_LNAout_ideal = beta1*(z1+z2)+noise*sqrt(10^((pow_Noise_dB)/10));
%% inverse distortion algorithm: does not work!

% for ii=1:6e3
%     temp = roots([beta3,0,beta1,-abs(y_LNAout(ii))]);
%     y_LNAout_inverse(ii) = y_LNAout(ii)/abs(y_LNAout(ii))*temp(1);
% end
% y_inverse_Fdomain = 20*log10(abs(fft(y_LNAout_inverse))/6e3);
% figure(88)
% plot([y_inverse_Fdomain(3001:6000),y_inverse_Fdomain(1:3000)])
% hold on
%     
%% quick PSD watch

% temp = 20*log10(abs(fft(y_LNAout_ideal(1:FFT_size).*hann(FFT_size)))/FFT_size);
% figure(88)
% plot(linspace(0,100,FFT_size),[temp(FFT_size/2+1:end);temp(1:FFT_size/2)]-41,'b')
% grid on
% ylim([-180,-60])
% xlabel('Frequency (MHz)')
% ylabel('Power Density (dBm/Hz)')
% title('Non-compensated PSD')

%% activate two or three BLKs
    
bhi = fir1(200,0.2,'low');
temp = filter(bhi,1,y_LNAout.*conj(carrier1));
z1_tilde_filter = temp(101:FFT_size+100).*carrier1(1:FFT_size);

% bhi = fir1(200,0.22,'low');
temp = filter(bhi,1,y_LNAout.*conj(carrier2));
z2_tilde_filter = temp(101:FFT_size+100).*carrier2(1:FFT_size);
    
% temp = filter(bhi,1,y_LNAout.*conj(carrier3));
% z3_tilde_filter = temp(101:61e2).*carrier3(1:6e3);


%% quick PSD from LPF to watch IMD around blockers

% temp9 = 20*log10(abs(fft(z1_tilde_filter)/6e3));
% figure
% plot([temp9(3001:end);temp9(1:3000)],'b');hold on
% temp9 = 20*log10(abs(fft(z2_tilde_filter)/6e3));
% plot([temp9(3001:end);temp9(1:3000)],'r')

%% get an idea how to make time alignment after LPF
    
%     z1_tilde_theo = beta1*z1+beta3*z1.*(abs(z1).^2+abs(z2).^2);
%     z2_tilde_theo = beta1*z2+beta3*z2.*(abs(z1).^2+abs(z2).^2);
%     
%     figure
%     plot(abs((z2_tilde_theo(1:5e2))),'b');hold on
%     plot(abs((z2_tilde_filter(1e2:6e2))),'r');hold on
%     figure
%     plot(20*log10(abs(fft(y_LNAout))/length(y_LNAout)),'r');hold on
%     
    %%
    
%     s_n0 = (abs(z1_tilde_filter).^2+abs(z2_tilde_filter).^2)*3*beta3;
    s_n = (abs(z1_tilde_filter).^2)*3*beta3/2;

    k0 = zeros(FFT_size,1);
    k1 = zeros(FFT_size,1);
    k2 = zeros(FFT_size,1);
%     k3 = zeros(FFT_size,1);
    
    for jj=1:FFT_size
        [k1(jj),k2(jj)] = block_estimation_v1(beta1,beta3,abs(z1_tilde_filter(jj)),abs(z2_tilde_filter(jj)));
    end
    
    
%     k3 = beta1+3*beta3*(abs(z1_tilde_filter)./k1).^2;
    
    z0_tilde_recons1 = (z1_tilde_filter./k1).*(3/2*beta3*abs(z1_tilde_filter./k1).^2+3*beta3*abs(z2_tilde_filter./k2).^2);
    z0_tilde_recons2 = (z2_tilde_filter./k2).*(3/2*beta3*abs(z2_tilde_filter./k2).^2+3*beta3*abs(z1_tilde_filter./k1).^2);
    z0_tilde_recons21 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
    z0_tilde_recons12 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);
%     z0_tilde_recons23 = 3/2*beta3*(z2_tilde_filter./k2).^2.*conj(z3_tilde_filter./k3);
%     z0_tilde_recons32 = 3/2*beta3*(z3_tilde_filter./k3).^2.*conj(z2_tilde_filter./k2);
%     z0_tilde_recons13 = 3/2*beta3*(z1_tilde_filter./k1).^2.*conj(z3_tilde_filter./k3);

%     z0_tilde_recons21_close = 3*beta3/2*(z2_tilde_filter).^2.*conj(z1_tilde_filter)./k0.^3;
%     z0_tilde_recons12_close = 3*beta3/2*(z1_tilde_filter).^2.*conj(z2_tilde_filter)./k0.^3;
    
    %% theta from LMS filter
    
%     stepsize = 6e10;
    stepsize = 1e10;
    LMS_ite_num = 200;
    
    % LMS for CMD 112
    vofn = 3*beta3/2*z1.*z1.*conj(z2);
    uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,LMS_ite_num);
    z0_tilde_recons12_LMS = thetaLMS(end)'*uofn;

    % LMS for CMD 221
    vofn = 3*beta3/2*z2.*z2.*conj(z1);
    uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,LMS_ite_num);
    z0_tilde_recons21_LMS = thetaLMS(end)'.*uofn;
    
    % LMS for CMD 111
    vofn = 3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2));
    uofn = 3/2*z1_tilde_filter.*(z1_tilde_filter.*conj(z1_tilde_filter)+...
        2*z2_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,LMS_ite_num);
    z_tilde_recons11_LMS = thetaLMS(end)'.*uofn;
    
    % LMS for CMD 222
    vofn = 3*beta3*z2.*(0.5*z2.*conj(z2)+z1.*conj(z1));
    uofn = 3/2*z2_tilde_filter.*(z2_tilde_filter.*conj(z2_tilde_filter)+...
        2*z1_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
    thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,LMS_ite_num);
    z_tilde_recons22_LMS = thetaLMS(end)'.*uofn;
    
%     % LMS for CMD 223
%     vofn = 3*beta3/2*z2.*z2.*conj(z3);
%     uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons23_LMS = thetaLMS(1:FFT_size)'.*uofn;
% 
%     % LMS for CMD 332
%     vofn = 3*beta3/2*z3.*z3.*conj(z2);
%     uofn = 3/2*(z3_tilde_filter.*z3_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons32_LMS = thetaLMS(1:FFT_size)'.*uofn;
% 
%     % LMS for CMD 113
%     vofn = 3*beta3/2*z1.*z1.*conj(z3);
%     uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z3_tilde_filter))./beta1.^3;
%     thetaLMS = CMD_LMS_v1(vofn,uofn,stepsize,beta3,FFT_size);
%     z0_tilde_recons13_LMS = thetaLMS(1:6e3)'.*uofn;

   
    %
%     k_true = beta1+beta3*(abs(z1).^2+abs(z2).^2);
%     orig_term = z0_tilde_filter(101:61e2);
%     cancel_term32 = beta3*z0_tilde_recons32./(k(1:60e2).^3);
%     cancel_term23 = beta3*z0_tilde_recons23./(k(1:60e2).^3);
%     cancel_term13 = beta3*z0_tilde_recons13./(k(1:60e2).^3);
    
    cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
%     cancel_term_close = z0_tilde_recons21_close+z0_tilde_recons12_close;
    cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS+...
        z_tilde_recons11_LMS+z_tilde_recons22_LMS;
%     cancel_term = cancel_term21+cancel_term12+cancel_term23+cancel_term32+cancel_term13;
%     figure
%     plot(abs(orig_term));hold on
%     plot(abs(cancel_term1))
    
%     figure
%     plot(20*log10(abs(fft(orig_term))/6e3));hold on
%     plot(20*log10(abs(fft(orig_term-cancel_term1-cancel_term2))/6e3),'r');hold on
%     grid on
%     
%     figure
%     temp = 20*log10(abs(fft(orig_term-cancel_term1)/6e3));
%     plot([temp(3001:end);temp(1:3000)],'r');hold on
%     temp = 20*log10(abs(fft(cancel_term2))/6e3);
%     plot([temp(3001:end);temp(1:3000)],'g');hold on

%% figure plotting
%     FFT_size = 500;

    figure
    subplot(211)
    y_uncomp_Fdomain = 20*log10(abs(fft(y_LNAout(1:FFT_size).*hann(FFT_size))/FFT_size));
    plot(linspace(-100,100,FFT_size),-41+[y_uncomp_Fdomain(FFT_size/2+1:end);y_uncomp_Fdomain(1:FFT_size/2)])
    grid on
    ylim([-180,-60])
    xlabel('Frequency (MHz)')
    ylabel('PSD (dBm/Hz)')
    title('Non-compensated PSD')
    
    
%     subplot(212)
%     y_comp_Fdomain = 20*log10(abs(fft((y_LNAout_ideal(1:6e3)).*hann(6e3)))/6e3);
%     plot([y_comp_Fdomain(3001:end);y_comp_Fdomain(1:3000)],'k')
%     hold on
%     grid on
%     ylim([-140,-20])
%     title('PSD from ideal LNA')
    
    subplot(212)
    y_comp_Fdomain = 20*log10(abs(fft((y_LNAout(1:FFT_size)-cancel_term(1:FFT_size)).*hann(FFT_size)))/FFT_size);
    plot(linspace(-100,100,FFT_size),-41+[y_comp_Fdomain(FFT_size/2+1:end);y_comp_Fdomain(1:FFT_size/2)],'b')
    hold on
    
    subplot(212)
    y_comp_Fdomain = 20*log10(abs(fft((y_LNAout(1:FFT_size)-cancel_term_LMS(1:FFT_size)).*hann(FFT_size)))/FFT_size);
    plot(linspace(-100,100,FFT_size),-41+[y_comp_Fdomain(FFT_size/2+1:end);y_comp_Fdomain(1:FFT_size/2)],'g')
    hold on
    grid on
    ylim([-180,-60])
    xlabel('Frequency (MHz)')
    ylabel('PSD (dBm/Hz)')
    legend('proposed method','LMS')
    title('Compensated PSD') 
  
%% watch for equivalent theta in
% 1) optimal compensation
% 2) proposed method
% 3) LMS

beta3_eq = beta3*(beta1./(beta1+3*beta3*(1/2*abs(z1.*conj(carrier1)).^2+abs(z2.*conj(carrier2)).^2))).^2.*(beta1./(beta1+3*beta3*(1/2*abs(z2.*conj(carrier2)).^2+abs(z1.*conj(carrier1)).^2)));
theta_mymethod = beta3*beta1^3./k1.^2./k2;
figure(99)
plot(10+10*log10(-4*beta1/3./beta3_eq),'b')
hold on
plot(10+10*log10(-4*beta1/3./theta_mymethod),'r')
hold on
plot(10+10*log10(-4*beta1/3./thetaLMS),'g--','linewidth',1)




