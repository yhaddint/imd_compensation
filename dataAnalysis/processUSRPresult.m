%%%%%% Script to process USRP raw test data for IMD Compensation %%%%%%

%% Setup environment
clear all;
% pkg load signal
% Add path for PSD plot function (from Xusong)
%addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IMD-Compensation-Source\IMD-Compensation\MATLAB');
% Add path for IQ imbalance compensation functions (from Han)
addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IQ-Imbalance');
% Add path for IMD compensation functions (TODO)
addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IMD-Compensation-Sim\functions');

%% Constants
FREQ_BLOCKER = 2.399e9;
FREQ_SIGNAL = 2.401e9;
FREQ_CENTER = 2.400e9;
SPAN = 10e6;

f_lower = (FREQ_CENTER - SPAN/2)/1e9;
f_upper = (FREQ_CENTER + SPAN/2)/1e9;

%% Pull data from files
data_17 = read_complex_binary ('C:\Users\Benjamin\Desktop\UCLA\CORES\IMDtestData\data_17db.dat');
data_18 = read_complex_binary ('C:\Users\Benjamin\Desktop\UCLA\CORES\IMDtestData\data_18db.dat');

%% Remove the DC component
data_17 = data_17 - mean(data_17); % real?
data_18 = data_18 - mean(data_18); 

START_N_SAMPLE = 5e4;
CROP_LEN = 1e4;
data_17_crop = data_17(START_N_SAMPLE:START_N_SAMPLE+CROP_LEN-1); % Reduced length for faster processing
data_18_crop = data_18(START_N_SAMPLE:START_N_SAMPLE+CROP_LEN-1);

% Plot FFT
plot17 = false;
plot18 = true;

if plot17
  figure;
  plot_fftMag(data_17_crop, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('17 dB Data Raw FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
end

if plot18 
  figure;
  plot_psd(data_18_crop, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('18 dB Data Raw FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
end

% IQ Imbalance compensation and plot compensated spectrum
[gm_hat_17, phim_hat_17] = run_IQest(data_17_crop, size(data_17_crop, 1));
[gm_hat_18, phim_hat_18] = run_IQest(data_18_crop, size(data_18_crop, 1));
% [data_17_IQcomp] = run_IQcomp(data_17_crop, gm_hat_17, phim_hat_17);
% [data_18_IQcomp] = run_IQcomp(data_18_crop, gm_hat_18, phim_hat_18);
data_17_IQcomp = data_17_crop;
data_18_IQcomp = data_18_crop;


if plot17
  figure;
  data_17_IQcomp_psd = plot_fftMag(data_17_IQcomp, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('17 dB Data IQ Compensated PSD');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
end

if plot18 
  figure;
  data_18_IQcomp_psd = plot_fftMag(data_18_IQcomp, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('18 dB Data IQ Compensated FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
end


% Determine the channel power for the blocker, modulated signal, and IMD feature
CHANNEL_BW = 0.5e6;

fIndex_blocker = (FREQ_BLOCKER - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_signal = (FREQ_SIGNAL - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_imd1 = (2*FREQ_BLOCKER - FREQ_SIGNAL - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_imd2 = (2*FREQ_SIGNAL - FREQ_BLOCKER - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;

fIndex_channel_width = CHANNEL_BW*CROP_LEN/SPAN;
f_channel_div = SPAN/CROP_LEN;

if plot17
  blocker_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_blocker - fIndex_channel_width/2)...
                      :(fIndex_blocker + fIndex_channel_width/2))/10)));
  signal_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_signal - fIndex_channel_width/2)...
                      :(fIndex_signal + fIndex_channel_width/2))/10)));
  imd_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_imd1 - fIndex_channel_width/2)...
                      :(fIndex_imd1 + fIndex_channel_width/2))/10)));
  disp(strcat("17 dB Data Blocker Power: ", int2str(blocker_17_pwr), " dB"));
  disp(strcat("17 dB Data Signal Power: ", int2str(signal_17_pwr), " dB"));
  disp(strcat("17 dB Data IMD Power: ", int2str(imd_17_pwr), " dB"));
end

if plot18
  blocker_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_blocker - fIndex_channel_width/2)...
                      :(fIndex_blocker + fIndex_channel_width/2))/10)));
  signal_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_signal - fIndex_channel_width/2)...
                      :(fIndex_signal + fIndex_channel_width/2))/10)));
  imd1_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_imd1 - fIndex_channel_width/2)...
                      :(fIndex_imd1 + fIndex_channel_width/2))/10)));
  imd2_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_imd2 - fIndex_channel_width/2)...
                      :(fIndex_imd2 + fIndex_channel_width/2))/10)));
  disp(strcat("18 dB Data Blocker Power: ", int2str(blocker_18_pwr), " dB"));
  disp(strcat("18 dB Data Signal Power: ", int2str(signal_18_pwr), " dB"));
  disp(strcat("18 dB Data IMD1 Power: ", int2str(imd1_18_pwr), " dB"));
  disp(strcat("18 dB Data IMD2 Power: ", int2str(imd2_18_pwr), " dB"));

else
  return; % Kill program since the remainder requires the 18 dB data
end 

% Run the LMS IMD compensation algorithm
%TODO
beta3_hat1 = -sqrt(10^((blocker_18_pwr + blocker_18_pwr + signal_18_pwr - imd1_18_pwr)/10));
beta3_hat2 = -1;

P = 1e4;
FFT_size = 2^13;
blk_freq1 = -1; % Eff. frequency of blocker 1 in [MHz]
blk_freq2 = 1; % Eff. frequency of blocker 2 in [MHz]
IMD_freq1 = -3;
IMD_freq2 = 3;
USRP_BW = 10; % USRP data collection bandwidth in [MHz]

bhi = fir1(200,0.1,'low');
carrier = exp(1j*(blk_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_18_IQcomp.*conj(carrier));
z1_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);

% bhi = fir1(200,0.1,'low');
carrier = exp(1j*(blk_freq2/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_18_IQcomp.*conj(carrier));
z2_tilde_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);

% bhi = fir1(200,0.1,'low');
carrier = exp(1j*(IMD_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_18_IQcomp.*conj(carrier));
IMD1_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);

% bhi = fir1(200,0.15,'low');
carrier = exp(1j*(IMD_freq2/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_18_IQcomp.*conj(carrier));
IMD2_filter = temp(101:FFT_size+100).*carrier(1:FFT_size);
% 
% 


% LMS for IMD z1 * z1 * conj(z2)
stepsize1 = 2e1;
LMS_ite_num1 = 500;
vofn = IMD1_filter;
uofn = z1_tilde_filter .* z1_tilde_filter .* conj(z2_tilde_filter);
thetaLMS1 = IMD_LMS(vofn,uofn,stepsize1,beta3_hat1,LMS_ite_num1);
z0_tilde_recons12_LMS = thetaLMS1(end)'*uofn;

% LMS for IMD z2 * z2 * conj(z1)
stepsize2 = 2e1;
LMS_ite_num2 = 500;
vofn = IMD2_filter;
uofn = z2_tilde_filter .* z2_tilde_filter .* conj(z1_tilde_filter);
thetaLMS2 = IMD_LMS(vofn,uofn,stepsize2,beta3_hat2,LMS_ite_num2);
z0_tilde_recons21_LMS = thetaLMS2(end)'*uofn;

comp_term = z0_tilde_recons12_LMS + z0_tilde_recons21_LMS;

sig_pre_comp_freq = fft((data_18_IQcomp(1:FFT_size)).*hann(FFT_size));
sig_post_comp_freq = fft((data_18_IQcomp(1:FFT_size) - comp_term).*hann(FFT_size));
%
figure
subplot(121)
plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([sig_pre_comp_freq(FFT_size/2+1:end);sig_pre_comp_freq(1:FFT_size/2)])));hold on
title('Pre-compensated Spectrum');
grid on
xlabel('Freq. [GHz]')
ylabel('PSD [dB]')
title('No Compensation')
subplot(122)
plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([sig_post_comp_freq(FFT_size/2+1:end);sig_post_comp_freq(1:FFT_size/2)])));hold on
title('Post-compensated Spectrum');
grid on
xlabel('Freq. [GHz]')
ylabel('PSD [dB]')
title('Post-LMS Filtering')

%% debug PSD
figure;
blocker_span = (fIndex_blocker - fIndex_channel_width/2):...
                (fIndex_blocker + fIndex_channel_width/2);
signal_span = (fIndex_signal - fIndex_channel_width/2):...
                (fIndex_signal + fIndex_channel_width/2);
IMD_span = (fIndex_imd1 - fIndex_channel_width/2):...
            (fIndex_imd1 + fIndex_channel_width/2);

% plot(data_18_IQcomp_psd);hold on
% z1_tilde_freq = fft(z1_tilde_filter.*hann(FFT_size));
% z2_tilde_freq = fft(z2_tilde_filter.*hann(FFT_size));
% IMD1_freq = fft(IMD1_filter.*hann(FFT_size));
IMD2_freq = fft(IMD2_filter.*hann(FFT_size));
uofn_freq = fft(uofn.*hann(FFT_size));


% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([z1_tilde_freq(FFT_size/2+1:end);z1_tilde_freq(1:FFT_size/2)])));hold on
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([z2_tilde_freq(FFT_size/2+1:end);z2_tilde_freq(1:FFT_size/2)])));hold on
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([IMD1_freq(FFT_size/2+1:end);IMD1_freq(1:FFT_size/2)])));hold on
subplot(211)
plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([IMD2_freq(FFT_size/2+1:end);IMD2_freq(1:FFT_size/2)])));hold on

subplot(212)
plot(linspace(f_lower,f_upper,FFT_size),-40+20*log10(abs([uofn_freq(FFT_size/2+1:end);uofn_freq(1:FFT_size/2)])));hold on



% plot(blocker_span,data_18_IQcomp_psd(blocker_span));hold on
% plot(signal_span,data_18_IQcomp_psd(signal_span));hold on
% plot(IMD_span,data_18_IQcomp_psd(IMD_span))

