%% Read CSV files from IIO Oscilloscope

function v = read_IIOscope_csv (filename, count, normalizeDataScale)
  % Scaling constant
  MAX_SCALE = 2^(16-1); 
  
  % Argument Handling
  if (nargin < 3)
    normalizeDataScale = 0;
  end
  if (normalizeDataScale == 0)
    scale = 1;
  elseif (normalizeDataScale > MAX_SCALE)
    scale = MAX_SCALE;
  else
    scale = normalizeDataScale;
  end
  
  % Read file from csv
  rawfile = csvread(filename);
  if (nargin < 2)
    count = size(rawfile, 1);
  end
  
  % Reformat data
  v = (rawfile(1:count,1)/scale) + i*(rawfile(1:count,2)/scale);
end
