%%%%%% Script to process USRP raw test data for IMD Compensation %%%%%%

%% Setup environment
clear all; clc
% pkg load signal
% Add path for PSD plot function (from Xusong)
%addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IMD-Compensation-Source\IMD-Compensation\MATLAB');
% Add path for IQ imbalance compensation functions (from Han)
% addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IQ-Imbalance');
% Add path for IMD compensation functions (TODO)
% addpath('C:\Users\Benjamin\Desktop\UCLA\CORES\IMD-Compensation-Sim\functions');

addpath('C:\Users\Han\Google Drive\IMD Data');


%% Constants
FREQ_BLOCKER = 2.398e9;
FREQ_SIGNAL = 2.402e9;
FREQ_CENTER = 2.400e9;
SPAN = 30.72e6; %20e6;

f_lower = (FREQ_CENTER - SPAN/2)/1e9;
f_upper = (FREQ_CENTER + SPAN/2)/1e9;

%% Pull data from files
%data_17 = read_complex_binary ('C:\Users\Benjamin\Desktop\UCLA\CORES\IMDtestData\data_17db.dat');
%data_18 = read_complex_binary ('C:\Users\Benjamin\Desktop\UCLA\CORES\IMDtestData\data_18db.dat');
data_17_raw = read_IIOscope_csv ('C:\Users\Han\Google Drive\IMD Data\test27_constellation1.csv');
data_18_raw = read_IIOscope_csv ('C:\Users\Han\Google Drive\IMD Data\test3_constellation2.csv');

%% Remove the DC component
data_17 = (data_17_raw - mean(data_17_raw))/1e3; % real?
data_18 = (data_18_raw - mean(data_18_raw))/1e3; 

START_N_SAMPLE = 1;%5e4;
CROP_LEN = 1e4;
data_17_crop = data_17(START_N_SAMPLE:START_N_SAMPLE+CROP_LEN-1); % Reduced length for faster processing
data_18_crop = data_18(START_N_SAMPLE:START_N_SAMPLE+CROP_LEN-1);

% Plot FFT
plot17 = true;
plot18 = false;

if plot17
  figure;
  plot_fftMag(data_17_crop, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('17 dB Data Raw FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
  xlim([2.39,2.41])
end

if plot18 
  figure;
  plot_psd(data_18_crop, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('18 dB Data Raw FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
  xlim([2.39,2.41])
end

% IQ Imbalance compensation and plot compensated spectrum
[gm_hat_17, phim_hat_17] = run_IQest(data_17_crop, size(data_17_crop, 1));
[gm_hat_18, phim_hat_18] = run_IQest(data_18_crop, size(data_18_crop, 1));
% [data_17_IQcomp] = run_IQcomp(data_17_crop, gm_hat_17, phim_hat_17);
% [data_18_IQcomp] = run_IQcomp(data_18_crop, gm_hat_18, phim_hat_18);
data_17_IQcomp = data_17_crop;
data_18_IQcomp = data_18_crop;


if plot17
  figure;
  data_17_IQcomp_psd = plot_fftMag(data_17_IQcomp, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('17 dB Data IQ Compensated PSD');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
  xlim([2.39,2.41])
end

if plot18 
  figure;
  data_18_IQcomp_psd = plot_fftMag(data_18_IQcomp, f_lower, f_upper);
  xlim([f_lower, f_upper]);
  title('18 dB Data IQ Compensated FFT');
  xlabel('Frequency (GHz)');
  ylabel('FFT Magnitude (dB)');
  xlim([2.39,2.41])
end


% Determine the channel power for the blocker, modulated signal, and IMD feature
CHANNEL_BW = 0.5e6;

fIndex_blocker = (FREQ_BLOCKER - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_signal = (FREQ_SIGNAL - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_imd1 = (2*FREQ_BLOCKER - FREQ_SIGNAL - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;
fIndex_imd2 = (2*FREQ_SIGNAL - FREQ_BLOCKER - FREQ_CENTER)*CROP_LEN/SPAN + CROP_LEN/2;

fIndex_channel_width = CHANNEL_BW*CROP_LEN/SPAN;
f_channel_div = SPAN/CROP_LEN;

if plot17
  blocker_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_blocker - fIndex_channel_width/2)...
                      :(fIndex_blocker + fIndex_channel_width/2))/10)));
  signal_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_signal - fIndex_channel_width/2)...
                      :(fIndex_signal + fIndex_channel_width/2))/10)));
  imd1_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_imd1 - fIndex_channel_width/2)...
                      :(fIndex_imd1 + fIndex_channel_width/2))/10)));
  imd2_17_pwr = 10*log10(f_channel_div * sum(10.^(data_17_IQcomp_psd...
                      ((fIndex_imd2 - fIndex_channel_width/2)...
                      :(fIndex_imd2 + fIndex_channel_width/2))/10)));
  disp(strcat("17 dB Data Blocker Power: ", int2str(blocker_17_pwr), " dB"));
  disp(strcat("17 dB Data Signal Power: ", int2str(signal_17_pwr), " dB"));
  disp(strcat("17 dB Data IMD1 Power: ", int2str(imd1_17_pwr), " dB"));
  disp(strcat("17 dB Data IMD2 Power: ", int2str(imd2_17_pwr), " dB"));

  xlim([2.39,2.41])
end

if plot18
  blocker_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_blocker - fIndex_channel_width/2)...
                      :(fIndex_blocker + fIndex_channel_width/2))/10)));
  signal_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_signal - fIndex_channel_width/2)...
                      :(fIndex_signal + fIndex_channel_width/2))/10)));
  imd1_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_imd1 - fIndex_channel_width/2)...
                      :(fIndex_imd1 + fIndex_channel_width/2))/10)));
  imd2_18_pwr = 10*log10(f_channel_div * sum(10.^(data_18_IQcomp_psd...
                      ((fIndex_imd2 - fIndex_channel_width/2)...
                      :(fIndex_imd2 + fIndex_channel_width/2))/10)));
  disp(strcat("18 dB Data Blocker Power: ", int2str(blocker_18_pwr), " dB"));
  disp(strcat("18 dB Data Signal Power: ", int2str(signal_18_pwr), " dB"));
  disp(strcat("18 dB Data IMD1 Power: ", int2str(imd1_18_pwr), " dB"));
  disp(strcat("18 dB Data IMD2 Power: ", int2str(imd2_18_pwr), " dB"));
  xlim([2.39,2.41])
else
%   return; % Kill program since the remainder requires the 18 dB data
end 

% Run the LMS IMD compensation algorithm
%% Get the BB part of all signals for further processing
% beta3_hat1 = -sqrt(10^((blocker_17_pwr + blocker_17_pwr + signal_17_pwr - imd1_17_pwr)/10));
% beta3_hat2 = -7000;

P = 1e4;
FFT_size = 2^13;
blk_freq1 = -2; % Eff. frequency of blocker 1 in [MHz]
blk_freq2 = 2; % Eff. frequency of blocker 2 in [MHz]
IMD_freq1 = -6;
IMD_freq2 = 6;
USRP_BW = 30.72; % USRP data collection bandwidth in [MHz]

filter_len = 2e2;
bhi = fir1(filter_len,0.1,'low');
carrier = exp(1j*(blk_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_17_IQcomp.*conj(carrier));
z1_tilde_BB = temp((filter_len/2+1):(FFT_size+filter_len/2));
z1_tilde_filter = temp((filter_len/2+1):(FFT_size+filter_len/2)).*carrier(1:FFT_size);

% bhi = fir1(200,0.1,'low');
carrier = exp(1j*(blk_freq2/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_17_IQcomp.*conj(carrier));
z2_tilde_BB = temp((filter_len/2+1):(FFT_size+filter_len/2));
z2_tilde_filter = temp((filter_len/2+1):(FFT_size+filter_len/2)).*carrier(1:FFT_size);

% bhi = fir1(200,0.1,'low');
carrier = exp(1j*(IMD_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_17_IQcomp.*conj(carrier));
IMD1_BB = temp((filter_len/2+1):(FFT_size+filter_len/2));
IMD1_filter = temp((filter_len/2+1):(FFT_size+filter_len/2)).*carrier(1:FFT_size);

% bhi = fir1(200,0.15,'low');
carrier = exp(1j*(IMD_freq2/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_17_IQcomp.*conj(carrier));
IMD2_BB = temp((filter_len/2+1):(FFT_size+filter_len/2));
IMD2_filter = temp((filter_len/2+1):(FFT_size+filter_len/2)).*carrier(1:FFT_size);
% 
% 
%% Estimate IM3 parameter (work with two-tone inputs)

% Plot for complex sample of tone (shifted to DC)
figure
subplot(311)
plot(z1_tilde_BB(100:10:end-1),'o')
grid on
axis([-0.5,0.5,-0.5,0.5])
title('BB part of BLK1')
xlabel('Real')
ylabel('Imag')
subplot(312)
plot(z2_tilde_BB(100:10:end-1),'o')
grid on
axis([-0.5,0.5,-0.5,0.5])
title('BB part of BLK2')
xlabel('Real')
ylabel('Imag')
subplot(313)
plot(IMD2_BB(100:10:end),'o')
grid on
axis([-0.02,0.02,-0.02,0.02])
title('BB part of IMD1')
xlabel('Real')
ylabel('Imag')

% Estimate Mag/Phase of Tones as well as IMD Tone
for ii=1:7500
    p1_hat(ii) = z1_tilde_BB(ii+filter_len/2+1).*conj(z1_tilde_BB(ii+filter_len/2));
end
z1_basis = exp(1j*(0:7500-1).'*phase(sum(p1_hat)));
c1 = pinv(z1_basis)*z1_tilde_BB((filter_len/2+1):(7500+filter_len/2));

for ii=1:7500
    p2_hat(ii) = z2_tilde_BB(ii+filter_len/2+1).*conj(z2_tilde_BB(ii+filter_len/2));
end
z2_basis = exp(1j*(0:7500-1).'*phase(sum(p2_hat)));
c2 = pinv(z2_basis)*z2_tilde_BB((filter_len/2+1):(7500+filter_len/2));

for ii=1:7500
    p3_hat(ii) = IMD1_BB(ii+filter_len/2+1).*conj(IMD1_BB(ii+filter_len/2));
end
z3_basis = exp(1j*(0:7500-1).'*phase(sum(p3_hat)));
c3 = pinv(z3_basis)*IMD1_BB((filter_len/2+1):(7500+filter_len/2));

% Heristically method to solve for IM3 parameter (beta1 = 1)
ite_max = 20;
a1 = zeros(ite_max,1);
a2 = zeros(ite_max,1);
b3 = zeros(ite_max,1);
a1(1) = c1;
a2(1) = c2;

for ff=1:ite_max-1
    b3(ff) = 2*c3/3/(a1(ff)^2*conj(a2(ff)));
    
    % ------ update a1 ----------
    ka1 = 3*b3(ff)*abs(a2(ff))^2;
    V_vec =    [(1+ka1+3*b3(ff)*abs(a1(ff))^2) + 1.5*b3(ff)*abs(a1(ff))^2,...
                (1+ka1+3*b3(ff)*abs(a1(ff))^2) - 1.5*b3(ff)*abs(a1(ff))^2];
    e1 = pinv(V_vec)*(c1 - a1(ff)*(1+ka1) - 1.5*b3(ff)*a1(ff)*abs(a1(ff))^2);
    a1(ff+1) = a1(ff) + e1(1) + 1j*e1(2);
    
    % ---------- update a2 ----------
    ka2 = 3*b3(ff)*abs(a1(ff))^2;
    V_vec =    [(1+ka2+3*b3(ff)*abs(a2(ff))^2) + 1.5*b3(ff)*abs(a2(ff))^2,...
                (1+ka2+3*b3(ff)*abs(a2(ff))^2) - 1.5*b3(ff)*abs(a2(ff))^2];
    e2 = pinv(V_vec)*(c2 - a2(ff)*(1+ka2) - 1.5*b3(ff)*a2(ff)*abs(a2(ff))^2);
    a2(ff+1) = a2(ff) + e2(1) + 1j*e2(2);
    
end

b3(end) = b3(end-1);

figure
semilogy(abs(c1 - a1.*(1 + 1.5*b3.*abs(a1).^2 + 3*b3.*abs(a2).^2)));hold on
semilogy(abs(c2 - a2.*(1 + 1.5*b3.*abs(a2).^2 + 3*b3.*abs(a1).^2)))
xlabel('Iteration Number')
ylabel('Metric of IM3 Pamameter Est.')
grid on

%% Compensation using proposed method
beta1_hat = 1;
beta3_hat = -0.1266; % Seems OK for all capture

k0 = zeros(FFT_size,1);
k1 = zeros(FFT_size,1);
k2 = zeros(FFT_size,1);

for jj=1:FFT_size
    [k1(jj),k2(jj)] = blk_reconstruct(beta1_hat,beta3_hat,abs(z1_tilde_filter(jj)),abs(z2_tilde_filter(jj)));
end

z0_tilde_recons1 = (z1_tilde_filter./k1).*(3/2*beta3_hat*abs(z1_tilde_filter./k1).^2+3*beta3_hat*abs(z2_tilde_filter./k2).^2);
z0_tilde_recons2 = (z2_tilde_filter./k2).*(3/2*beta3_hat*abs(z2_tilde_filter./k2).^2+3*beta3_hat*abs(z1_tilde_filter./k1).^2);
z0_tilde_recons21 = 3/2*beta3_hat*(z2_tilde_filter./k2).^2.*conj(z1_tilde_filter./k1);
z0_tilde_recons12 = 3/2*beta3_hat*(z1_tilde_filter./k1).^2.*conj(z2_tilde_filter./k2);

% cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
cancel_term = exp(1j*15/180*pi)*z0_tilde_recons12;
              %exp(1j*15/180*pi)*z0_tilde_recons21; % there is 15 deg phase offset 


% Plot time domain waveform of IMD and signal used for compensation
figure
carrier = exp(1j*(IMD_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,data_17_IQcomp.*conj(carrier));
IMD1_BB_new = temp(101:FFT_size);
% subplot(211)
plot(abs(IMD1_BB_new(1e3:1:7e3))); hold on
grid on
% ax_lim = max(abs(IMD1_BB_new(1e3:10:2e3)));
% axis([-ax_lim,ax_lim,-ax_lim,ax_lim])
carrier = exp(1j*(IMD_freq1/USRP_BW*2*pi*(0:P-1)'));
temp = filter(bhi,1,cancel_term.*conj(carrier(1:FFT_size)));
IMD1_BB_hat = temp(101:FFT_size);
% subplot(211)
plot(abs((IMD1_BB_hat(1e3:1:7e3))));
grid on
% ax_lim = max(abs(IMD1_BB(1e3:10:8e3)));
% axis([-ax_lim,ax_lim,-ax_lim,ax_lim])

% post-comp PSD plot 
comp_term = cancel_term;
figure
plot_fftMag(data_17_IQcomp(1:FFT_size/2) - comp_term(1:FFT_size/2), f_lower, f_upper);
% plot_fftMag(data_17_IQcomp(1:FFT_size/2), f_lower, f_upper);
% hold on
% plot_fftMag(comp_term(1:FFT_size/2), f_lower, f_upper);

set(gca,'FontSize',14)
xlabel('Frequency (GHz)');
ylabel('FFT Magnitude (dB)');
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([sig_post_comp_freq(FFT_size/2+1:end);sig_post_comp_freq(1:FFT_size/2)])));hold on
title('Post-compensated Spectrum');
grid on
xlim([2.39,2.41])
% figure
% plot(c1*z1_basis - z1_tilde_BB(101:8100))
% plot(c2*z2_basis - z2_tilde_BB(101:8100))
% plot(c3*z3_basis - IMD1_BB(101:8100));

%% Runing LMS filter for optimal compensation coeff.
% LMS for IMD z1 * z1 * conj(z2)
beta3_hat_LMS = beta3_hat;
stepsize1 = 2e1;
LMS_ite_num1 = 3e3;
vofn1 = IMD1_filter;
uofn1 = z1_tilde_filter .* z1_tilde_filter .* conj(z2_tilde_filter);

thetaLMS1 = IMD_LMS(vofn1,uofn1,stepsize1,beta3_hat_LMS,LMS_ite_num1);
z0_tilde_recons12_LMS = thetaLMS1(end)'*uofn1;
% 
% LMS for IMD z2 * z2 * conj(z1)
stepsize2 = 2e1;
LMS_ite_num2 = 3e3;
vofn2 = IMD2_filter;
uofn2 = z2_tilde_filter .* z2_tilde_filter .* conj(z1_tilde_filter);
thetaLMS2 = IMD_LMS(vofn2,uofn2,stepsize2,beta3_hatLMS,LMS_ite_num2);
z0_tilde_recons21_LMS = thetaLMS2(end)'*uofn2;

cancel_term_LMS = z0_tilde_recons21_LMS + z0_tilde_recons12_LMS;
% vofn21 = IMD2_filter;
% uofn21 = conj(z1_tilde_filter) .* conj(z1_tilde_filter) .* conj(z1_tilde_filter);
% uofn22 = (z2_tilde_filter) .* (z2_tilde_filter) .* (z2_tilde_filter);
% 
% uofn11 = conj(z2_tilde_filter) .* conj(z2_tilde_filter) .* conj(z2_tilde_filter);
% uofn12 = (z1_tilde_filter) .* (z1_tilde_filter) .* (z1_tilde_filter);

%%
carrier = exp(1j*IMD_freq1/USRP_BW*2*pi*(0:FFT_size-1)');
figure
subplot(211)
semilogy(abs(vofn1.*conj(carrier)));hold on
grid on
title('Original IMD Copy')
% ylim([1e-3,1e-2])
subplot(212)
semilogy(abs(uofn1.*conj(carrier)))
grid on
title('Compomemt to Match')
% ylim([1e-3,1e-2])
%%
figure
subplot(211)
semilogy(abs(vofn1)./abs(uofn1))
xlim([400,1000])
ylim([0.1,0.5])
grid on
subplot(212)
semilogy(abs(vofn1))
grid on
xlim([400,1000])

%%
figure
subplot(311)
semilogy(abs(z2_tilde_filter.*exp(1j*blk_freq2/USRP_BW*2*pi*(0:FFT_size-1)')));hold on
grid on


%% ------- Directly Using MMSE Estimator (offline, equivalent to LMS) -------------
% thetaLMS(1) = -0.1;
% for ii=1:FFT_size-1
%     thetaLMS(ii+1) = thetaLMS(ii)-2*stepsize2*(thetaLMS(ii)*abs(uofn(ii))^2-real(uofn(ii)*conj(vofn(ii))));
% end
% z0_tilde_recons21_LMS = thetaLMS(end)'*uofn
thetaMMSE00 = pinv([uofn1])*vofn1;
thetaMMSE01 = pinv([uofn2])*vofn2;

% thetaLMS031 = pinv(uofn31)*vofn3;
% thetaLMS032 = pinv(uofn32)*vofn3;
% thetaLMS04 = pinv(uofn4)*vofn4;


% --------------------------
% z0_tilde_recons21_LMS = thetaLMS00 * conj(vofn1);

% thetaLMS01 = pinv(uofn2) * (vofn2 - thetaLMS00 * conj(vofn1));

comp_term = [uofn1] * thetaMMSE00 + [uofn2]*thetaMMSE01;

sig_pre_comp_freq = fft((data_17_IQcomp(1:FFT_size)).*hann(FFT_size));
sig_post_comp_freq = fft((data_17_IQcomp(1:FFT_size) - comp_term).*hann(FFT_size));
%%
comp_term = cancel_term_LMS;
figure
subplot(211)
plot_fftMag(data_17_IQcomp(1:FFT_size), f_lower, f_upper);
set(gca,'FontSize',14)
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([sig_pre_comp_freq(FFT_size/2+1:end);sig_pre_comp_freq(1:FFT_size/2)])));hold on
xlabel('Frequency (GHz)');
ylabel('FFT Magnitude (dB)');
title('Pre-compensated Spectrum');
grid on
xlim([2.39,2.41])

subplot(212)
plot_fftMag(data_17_IQcomp(1:FFT_size) - comp_term, f_lower, f_upper);
% plot_fftMag(comp_term, f_lower, f_upper);

set(gca,'FontSize',14)
xlabel('Frequency (GHz)');
ylabel('FFT Magnitude (dB)');
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([sig_post_comp_freq(FFT_size/2+1:end);sig_post_comp_freq(1:FFT_size/2)])));hold on
title('Post-compensated Spectrum');
grid on
xlim([2.39,2.41])


%% debug PSD
figure;
blocker_span = (fIndex_blocker - fIndex_channel_width/2):...
                (fIndex_blocker + fIndex_channel_width/2);
signal_span = (fIndex_signal - fIndex_channel_width/2):...
                (fIndex_signal + fIndex_channel_width/2);
IMD_span = (fIndex_imd1 - fIndex_channel_width/2):...
            (fIndex_imd1 + fIndex_channel_width/2);

% plot(data_18_IQcomp_psd);hold on
z1_tilde_freq = fft(z1_tilde_filter.*hann(FFT_size));
% z2_tilde_freq = fft(z2_tilde_filter.*hann(FFT_size));
% IMD1_freq = fft(IMD1_filter.*hann(FFT_size));
% IMD2_freq = fft(IMD2_filter.*hann(FFT_size));
% uofn_freq = fft(uofn.*hann(FFT_size));

plot_fftMag(z1_tilde_filter, f_lower, f_upper);
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([z1_tilde_freq(FFT_size/2+1:end);z1_tilde_freq(1:FFT_size/2)])));hold on
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([z2_tilde_freq(FFT_size/2+1:end);z2_tilde_freq(1:FFT_size/2)])));hold on
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([IMD1_freq(FFT_size/2+1:end);IMD1_freq(1:FFT_size/2)])));hold on
% subplot(211)

xlim([f_lower, f_upper]);
title('17 dB Data IQ Compensated PSD');
xlabel('Frequency (GHz)');
ylabel('FFT Magnitude (dB)');
% plot(linspace(f_lower,f_upper,FFT_size),20*log10(abs([IMD2_freq(FFT_size/2+1:end);IMD2_freq(1:FFT_size/2)])));hold on
% subplot(212)
% plot(linspace(f_lower,f_upper,FFT_size),-40+20*log10(abs([uofn_freq(FFT_size/2+1:end);uofn_freq(1:FFT_size/2)])));hold on


% plot(blocker_span,data_18_IQcomp_psd(blocker_span));hold on
% plot(signal_span,data_18_IQcomp_psd(signal_span));hold on
% plot(IMD_span,data_18_IQcomp_psd(IMD_span))

