function IMDcomp_LMS(
%% theta from LMS filter
%     stepsize = 6e10;
stepsize = 1e10;
LMS_ite_num = 200;

% LMS for IMD z1 * z1 * conj(z2)
vofn = 3*beta3/2*z1.*z1.*conj(z2);
uofn = 3/2*(z1_tilde_filter.*z1_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z0_tilde_recons12_LMS = thetaLMS(end)'*uofn;

% LMS for IMD z2 * z2 * conj(z1)
vofn = 3*beta3/2*z2.*z2.*conj(z1);
uofn = 3/2*(z2_tilde_filter.*z2_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z0_tilde_recons21_LMS = thetaLMS(end)'.*uofn;

% LMS for IMD z1 * z1 * conj(z1)
vofn = 3*beta3*z1.*(0.5*z1.*conj(z1)+z2.*conj(z2));
uofn = 3/2*z1_tilde_filter.*(z1_tilde_filter.*conj(z1_tilde_filter)+...
        2*z2_tilde_filter.*conj(z2_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z_tilde_recons11_LMS = thetaLMS(end)'.*uofn;

% LMS for CMD z2 * z2 * conj(z2)
vofn = 3*beta3*z2.*(0.5*z2.*conj(z2)+z1.*conj(z1));
uofn = 3/2*z2_tilde_filter.*(z2_tilde_filter.*conj(z2_tilde_filter)+...
        2*z1_tilde_filter.*conj(z1_tilde_filter))./beta1.^3;
thetaLMS = IMD_LMS(vofn,uofn,stepsize,beta3,LMS_ite_num);
z_tilde_recons22_LMS = thetaLMS(end)'.*uofn;

cancel_term = z0_tilde_recons1+z0_tilde_recons2+z0_tilde_recons21+z0_tilde_recons12;
cancel_term_LMS = z0_tilde_recons12_LMS+z0_tilde_recons21_LMS+...
                    z_tilde_recons11_LMS+z_tilde_recons22_LMS;