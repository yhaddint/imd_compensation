function signal_psd = plot_psd( signal, x_bound_low, x_bound_up, config )

% Function to plot the PSD of a given signal
% Inputs:
%         signal - input signal
%         x_bound_low - lower label bound for x-axis
%         x_bound_up - upper label bound for x-axis
%         config - plot configuration parameter
% Outputs:
%         signal_psd - PSD of the signal (in FFT processing order
%                      i.e. high freq. at center of vector)

if (nargin < 4)
  config = '';
end
if (nargin < 3)
  x_bound_up = 50;
end
if (nargin < 2)
  x_bound_low = -50;
end
  
len = length(signal);
y_comp_Fdomain = 10*log10((abs(fft(signal.*hann(len))).^2)/(len));
plot(linspace(x_bound_low,x_bound_up,len),...
     [y_comp_Fdomain(len/2+1:end);y_comp_Fdomain(1:len/2)], config);
grid on;
signal_psd = y_comp_Fdomain;