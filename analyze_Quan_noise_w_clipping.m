%-------------------------------------
% Script control parameter
%-------------------------------------
clear; clc; warning off;
rand('seed',2)

%-------------------------------------
%           System Parameters
%-------------------------------------
SampleNumberAve = 5;                      % signal length control
P = 5e3;                                  % signal length control
sig_length = 50*P;                        % Signal length for processing
FFT_size = 2^13;                          % FFT size
UE_num = 8;                               % Total UE number in UL
UE_of_interest = 2;                       % index of UE that we evaluate
Nr = 64;                                  % antenna size
% sig_pow_dBm = -45;                      % Power of z1 in dBm
sig_pow_dBm = ones(1,UE_num)*(-51);
sig_pow_dBm(2) = -70;                     % Power of z1 in dBm
sig_phi = [-35,-25,-15,-5,5,15,25,35]'/180*pi;               % AoA of signal 1 in rad
% sig_phi = sig_phi+(rand(UE_num,1)*5-2.5)/180*pi;

MCtimes = 1;                            % Monte Carlo Simulation

% ----------    LNA parameter    -------------
% in 28GHz, Gain is [8.5 30 12.8], IIP3_dBm is [5,-1.5,5] dBm
gain_dB = 15;                             % LNA linear gain in dB
% IIP3_dBm = -10;                           % LNA 3rd order Input intercept point in dBm

IP3_num = 13;
IP3_range = linspace(-40,-16,IP3_num);    % LNA 3rd order Input intercept point in dBm


%-------------------------------------
% Generate Test Waveform (MC) in Baseband
%-------------------------------------
% [ OFDM_env_rx_norm ] = get_MC_waveform( P );
ADC_bits_range = 5:6;
dist_stream_normalized = zeros(length(ADC_bits_range),IP3_num);
dist_stream_normalized2 = zeros(length(ADC_bits_range),IP3_num);
dist_stream_BM = zeros(length(ADC_bits_range),IP3_num);
dist_stream_Nocomp = zeros(length(ADC_bits_range),IP3_num);

for ip3_idx = 1:IP3_num

% ----------- LNA parameters -------------------
IIP3_dBm = IP3_range(ip3_idx);
[beta1,beta3_neg] = get_beta(gain_dB,IIP3_dBm);   % LNA parameter
beta3 = -beta3_neg;
clip_level = dBm2V(IIP3_dBm-6);


for ii=1:UE_num
    sig_mag = dBm2V(sig_pow_dBm(ii));
%     sig_UE_raw(:,ii) = randn(P,1) + 1j*randn(P,1);
    sig_UE_raw(:,ii) = get_SC_waveform( 16, P, 5, 1 );
    sig_UE(:,ii) = sig_mag * sig_UE_raw(:,ii)./norm(sig_UE_raw(:,ii))*sqrt(P);
end

% ------ Input Signal of LNA -----------
spatial_res = exp(1j*pi*sin(sig_phi)*(0:(Nr-1))); % row - UE index, col - antenna index
sig_in = sig_UE * spatial_res;


% ------ LNA non-linearity model in each antenna (only cipping)-----------
y_LNAout_BM = beta1 * sig_in;
for ant_idx = 1:Nr
    y_LNAout(:,ant_idx) = beta1 * get_clipping( sig_in(:,ant_idx), clip_level);
end

% % ------ LNA non-linearity model (IP3 and clipping - not working) -----------
% y_LNAout_BM = beta1 * sig_in;
% for ant_idx=1:Nr
%     y_LNAout_temp(:,ant_idx) = get_clipping( sig_in(:,ant_idx), clip_level);
%     y_LNAout(:,ant_idx) = beta1 * y_LNAout_temp(:,ant_idx);%...
% %                    + beta3 * y_LNAout_temp(:,kk) .* abs(y_LNAout_temp(:,kk)).^2;
% end

%% check clip statistics
% for idx=1:P
%     clip_num(idx) = sum(abs(real(y_LNAout(idx,:)) - real(y_LNAout_BM(idx,:)))>1e-5);
% end
% figure
% hist(clip_num,16)
%% check spectrum with and without clipping
% figure;
% subplot(311)
% plot(real(y_LNAout(1:1e3,1) - y_LNAout_BM(1:1e3,1)))
% grid on
% subplot(312)
% plot(real(y_LNAout(1:1e3,2) - y_LNAout_BM(1:1e3,2)))
% grid on
% subplot(313)
% plot(real(y_LNAout(1:1e3,3) - y_LNAout_BM(1:1e3,3)))
% grid on

fft_output_BM = 20*log10(abs(fft(hann(P).*y_LNAout_BM(:,1))));
fft_output = 20*log10(abs(fft(hann(P).*y_LNAout(:,1))));
fft_output_error = 20*log10(abs(fft(hann(P).*(y_LNAout(:,1)-y_LNAout_BM(:,1)))));

% figure
% subplot(311)
% plot([fft_output(P/2+1:end);fft_output(1:P/2)]);
% grid on
% subplot(312)
% plot([fft_output_BM(P/2+1:end);fft_output_BM(1:P/2)]);
% grid on
% subplot(313)
% plot([fft_output_error(P/2+1:end);fft_output_error(1:P/2)]);
% grid on


%% Main processing block

samp_count = zeros(length(ADC_bits_range),1);
for bits_idx = 1:length(ADC_bits_range)
    
    ADC_bits = ADC_bits_range(bits_idx);
    fprintf('IP3 = %d dBm, ADC Quan = %d Bits\n',IIP3_dBm, ADC_bits);
    
    max_mag = zeros(Nr,1);
    for ant_idx = 1:Nr
        
        % ------ ADC Quantization after NL amplifier -----------
        max_mag(ant_idx) = max(max(abs(real(y_LNAout(:,ant_idx)))),max(abs(imag(y_LNAout(:,ant_idx)))));
        beta_range = linspace(1,1,1); % ADC loading fraction
        
        % Using different ADC loading fraction (exhaustive test)
        for beta_idx = 1:1
            max_ADC_V = beta_range(beta_idx) * max_mag(ant_idx);%max_mag(kk);
            y_ADCout_beta(:,beta_idx) = ADC_overload( y_LNAout(:,ant_idx), ADC_bits, max_ADC_V );
            Quan_Pow(beta_idx) = norm(y_ADCout_beta(:,beta_idx) - y_LNAout(:,ant_idx));
        end
        [~,beta_best] = min(Quan_Pow); % Heuristically pick best ADC loading
        y_ADCout(:,ant_idx) = y_ADCout_beta(:,beta_best);
        
        % ------ ADC Quantization after benchmark (BM) linear amplifier -----------
        max_mag_BM(:,ant_idx) = max(max(abs(real(y_LNAout_BM(:,ant_idx)))),max(abs(imag(y_LNAout_BM(:,ant_idx)))));
        beta_range = linspace(0.3,1.2,15); % ADC loading fraction
        for beta_idx = 1:15 % Sweep loading fraction value
            max_ADC_V = beta_range(beta_idx) * max_mag_BM(ant_idx);
            y_ADCout_BM_beta(:,beta_idx) = ADC_overload( y_LNAout_BM(:,ant_idx), ADC_bits, max_ADC_V );
            Quan_Pow_BM(beta_idx) = norm(y_ADCout_BM_beta(:,beta_idx) - y_LNAout_BM(:,ant_idx));
        end
        [~,beta_best_BM] = min(Quan_Pow_BM); % Heuristically pick best ADC loading
        y_ADCout_BM(:,ant_idx) = y_ADCout_BM_beta(:,beta_best_BM);
        
    end
    
    % --------- proposed compensation ----------
    % comp is replace clipped samples with "Genie" samples
    % comp2 is useless
    % comp3 is proposed method
    
%     top_mag(:,bits_idx) = max_mag - max_mag/(2^ADC_bits);
    y_ADCout_comp = y_ADCout; % Initialization
    
    null_mtx = (null(spatial_res))';
    
    for pp=1:P
%         pick_idx = find(abs(y_ADCout(pp,:))>(1.02*top_mag(:,bits_idx))');

        % Thresholding on samples that could be clipped
        pick_idx = find(abs(y_ADCout(pp,:))>(0.95*clip_level*beta1*ones(64,1))');

        y_ADCout_comp2(pp,:) = y_ADCout(pp,:); % Initialization
        y_ADCout_comp3(pp,:) = y_ADCout(pp,:); % Initialization
        
        if length(pick_idx)
            % ------ideal comp. (replace w. non-clipped samples) -------
            y_ADCout_comp(pp,pick_idx) = y_ADCout_BM(pp,pick_idx);
            
            % ------ proposed comp (null-space power minimization) ------
            correct_idx = setdiff(1:64,pick_idx);
            LS_y = -null_mtx(:,correct_idx)*(y_ADCout(pp,correct_idx)');
            LS_y2 = -null_mtx*(y_ADCout(pp,:)');
            LS_A = null_mtx(:,pick_idx);
            beta = 0.005;
%             y_ADCout_comp2(pp,pick_idx) = (pinv(LS_A)*LS_y)';
            y_ADCout_comp3(pp,pick_idx) = y_ADCout(pp,pick_idx) + ...
                        (inv(LS_A'*LS_A+beta * eye(length(pick_idx)))*LS_A'*LS_y2)';

        end
        
        % counting how many samples get clipped
        samp_count(bits_idx) = samp_count(bits_idx) + length(pick_idx);
    end
    
    
%     % LUT compensation for IIP3
%     for kk = 1:Nr
%         % ------ Compensation Design (Look-Up Table) ---------
%         useful_idx = find(abs(sig_in(:,kk))<clip_level);
%         LUT = [abs(sig_in(useful_idx,kk)).^2, abs(y_LNAout(useful_idx,kk)).^2];
%         
%         % ----- LUT Compensation Execution ---------
%         y_post_IP3comp(:,kk) = zeros(P,1);
%         for pp=1:P
%             if abs(sig_in(pp,kk))<clip_level
%             [~,idx] = min(abs(abs(y_ADCout_comp(pp,kk))^2 - LUT(:,2)));
%             y_post_IP3comp(pp,kk) = y_ADCout_comp(pp,kk) / sqrt(LUT(idx,2)) * sqrt(LUT(idx,1));
%             else
%                 y_post_IP3comp(pp,kk) = y_ADCout_comp(pp,kk)/beta1;
%             end
%         end
%     end
    
    % ------test whehter clipping can be recovered by spatial ---------
    if 0
%         new_angles = -60;
        new_angle_res1 = null(spatial_res);
%         null_res_temp = pinv(new_angle_res1);
%         null_res = null_res_temp(:,UE_num+1);
        pow_ideal = mean(abs(y_ADCout_BM*(new_angle_res1)).^2,1);
        pow_raw = mean(abs(y_ADCout*(new_angle_res1)).^2,1); 
        pow_comp = mean(abs(y_ADCout_comp*(new_angle_res1)).^2,1); 
%         pow_comp2 = mean(abs(y_ADCout_comp2*(new_angle_res1)).^2,1); 
        pow_comp3 = mean(abs(y_ADCout_comp3*(new_angle_res1)).^2,1); 

%         10*log10(pow_raw)
%         10*log10(pow_comp)
        
        figure
        plot(10*log10(pow_raw),'linewidth',2);hold on
        plot(10*log10(pow_comp),'linewidth',2);hold on
%         plot(10*log10(pow_comp2),'linewidth',2);hold on
        plot(10*log10(pow_comp3),'linewidth',2);hold on
        plot(10*log10(pow_ideal),'linewidth',2);hold on
        grid on
        xlabel('Null Space Index')
        ylabel('Power [dB]')
        legend('Raw','Genie Comp','Proposed Comp','No Clipping')
        
    end
    
    % test whether spectrum information can be used to
%     recover clopping
%     if ADC_bits==4 
%         fft_output_BM = 20*log10(abs(fft(hann(P).*y_ADCout_BM(:,1))));
%         fft_output = 20*log10(abs(fft(hann(P).*y_ADCout(:,1))));
%         fft_output_error = 20*log10(abs(fft(hann(P).*(y_ADCout(:,1)-y_ADCout_BM(:,1)))));
%         fft_output_comp_diff = 20*log10(abs(fft(hann(P).*...
%                                     (y_ADCout(:,1)-y_ADCout_comp(:,1)))));
% 
%         
%         figure
%         plot(beta1*real(sig_in(:,1)),real(y_ADCout_comp(:,1)),'.');hold on
%         plot(beta1*real(sig_in(:,1)),real(y_ADCout(:,1)),'.');hold on
%         plot(beta1*imag(sig_in(:,1)),imag(y_ADCout_BM(:,1)),'.');hold on
%         grid on
%         legend('comp','raw','ideal')
%         
%         
%         figure
%         subplot(221)
%         plot([fft_output(P/2+1:end);fft_output(1:P/2)]);
%         grid on
%         subplot(222)
%         plot([fft_output_BM(P/2+1:end);fft_output_BM(1:P/2)]);
%         grid on
%         subplot(223)
%         plot([fft_output_error(P/2+1:end);fft_output_error(1:P/2)]);
%         grid on
%         subplot(224)
%         plot([fft_output_comp_diff(P/2+1:end);fft_output_comp_diff(1:P/2)]);
%         grid on
%     end
    
    % ------ MIMO detection ----------
    Rx_filter_angle = sig_phi(UE_of_interest);
    for ii = 1 : UE_num
        H_MIMO(:,ii) = exp(1j * pi * sin(sig_phi(ii)) * (0:(Nr-1)).');
    end
    RBF_ZF = pinv(H_MIMO);
    RBF_ZF_normal = RBF_ZF./norm(RBF_ZF,'fro');
    Rx_spatial_filter = RBF_ZF_normal(UE_of_interest,:).';
    y_MIMOout_BM = y_ADCout_BM * Rx_spatial_filter;
    y_MIMOout_nocomp = y_ADCout * Rx_spatial_filter;
    y_MIMOout_comp = y_ADCout_comp * Rx_spatial_filter;
    y_MIMOout_comp3 = y_ADCout_comp3 * Rx_spatial_filter;

    
    
    % ----- Evaluation (Pre-MIMO-detection)---------
    ant_of_interest = 2;
    
    alpha_BM = pinv(sig_in(:,ant_of_interest))*(y_ADCout_BM(:,ant_of_interest)/beta1);
    dist_ant_BM(bits_idx) = norm((y_ADCout_BM(:,ant_of_interest)/beta1) -...
        alpha_BM*sig_in(:,ant_of_interest))^2/norm(alpha_BM*sig_in(:,ant_of_interest))^2;
    
%     alpha_comp = pinv(sig_in(:,ant_of_interest))*(y_post_IP3comp(:,ant_of_interest)/beta1);
%     dist_ant_comp(bits_idx) = norm((y_post_IP3comp(:,ant_of_interest)/beta1) -...
%         alpha_comp*sig_in(:,ant_of_interest))^2/norm(alpha_comp*sig_in(:,ant_of_interest))^2;
    
    alpha_nocomp = pinv(sig_in(:,ant_of_interest))*(y_ADCout(:,ant_of_interest));
    dist_ant_Nocomp(bits_idx) = norm(y_ADCout(:,ant_of_interest) -...
        alpha_nocomp*sig_in(:,ant_of_interest))^2/norm(alpha_nocomp*sig_in(:,ant_of_interest))^2;  
    
    
    % ----- Evaluation (Post-MIMO-detection)---------
    
    alpha_BM = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_BM/beta1);
    dist_stream_BM(bits_idx,ip3_idx) = norm((y_MIMOout_BM/beta1) - alpha_BM*sig_UE(:,UE_of_interest))^2/norm(alpha_BM*sig_UE(:,UE_of_interest))^2;
    
    alpha_nocomp = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_nocomp);
    dist_stream_Nocomp(bits_idx,ip3_idx) = norm(y_MIMOout_nocomp - alpha_nocomp*sig_UE(:,UE_of_interest))^2/norm(alpha_nocomp*sig_UE(:,UE_of_interest))^2;

    alpha_comp = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_comp);
    dist_stream_comp(bits_idx,ip3_idx) = norm(y_MIMOout_comp - alpha_comp*sig_UE(:,UE_of_interest))^2/norm(alpha_comp*sig_UE(:,UE_of_interest))^2;

    alpha_comp3 = pinv(sig_UE(:,UE_of_interest))*(y_MIMOout_comp3);
    dist_stream_comp3(bits_idx,ip3_idx) = norm(y_MIMOout_comp3 - alpha_comp3*sig_UE(:,UE_of_interest))^2/norm(alpha_comp3*sig_UE(:,UE_of_interest))^2;

end
end
%%
% figure
% % h2 = plot(ADC_bits_range,10*log10(dist_ant_comp),'-.s');
% % hold on
% h3 = plot(ADC_bits_range,10*log10(dist_ant_Nocomp),'-.s');
% hold on
% h4 = plot(ADC_bits_range,10*log10(dist_ant_BM),'--x');
% hold on
% title('Antenna-Level Distortion')
% set([h3,h4],'linewidth',2);
% set([h3,h4],'markersize',8);
% 
% set(gca,'FontSize',14)
% 
% grid on
% xlabel('ADC Bits')
% ylabel('Normalized Distortion [dB]')
% legend('comp', 'No Comp.','Linear')
%% Performance v.s. ADC bits
figure
% h1 = plot(ADC_bits_range,10*log10(dist_stream_comp),'-.s');
% hold on
h2 = plot(ADC_bits_range,10*log10(dist_stream_comp3),'-.s');
hold on
h3 = plot(ADC_bits_range,10*log10(dist_stream_Nocomp),'-.s');
hold on
h4 = plot(ADC_bits_range,10*log10(dist_stream_BM),'--x');
hold on
title('Stream-Level Distortion')
set([h2,h3,h4],'linewidth',2);
set([h2,h3,h4],'markersize',8);

set(gca,'FontSize',14)

grid on
xlabel('ADC Bits')
ylabel('Norm. Distortion [dB]')
legend('NL, Prop. Comp.','NL, No Comp.','Linear')
%% Performance v.s. IIP3 of amplifier 
figure
% h1 = plot(ADC_bits_range,10*log10(dist_stream_comp),'-.s');
% hold on
h2 = plot(IP3_range,10*log10(dist_stream_comp3(2,:)),'-.s');
hold on
h3 = plot(IP3_range,10*log10(dist_stream_Nocomp(2,:)),'-.s');
hold on
% h4 = plot(IP3_range,10*log10(dist_stream_BM),'--x');
% hold on
title('Stream-Level Distortion')
set([h2,h3],'linewidth',2);
set([h2,h3],'markersize',8);

set(gca,'FontSize',14)

grid on
xlabel('LNA IIP3 [dBm]')
ylabel('Norm. Distortion [dB]')
legend('NL, Prop. Comp.','NL, No Comp.')
xticks(linspace(-40,-16,7))

